(*) error that we may encounter:
    - Instrumentation does not compile: pay attention to dependencies.
      Also, see note on static methods and static variables bellow
    - Illegal environment setting in parser: some dependency are inappropriately set, e.g.,
      set to null or non-exist folder

* check if a field is visible at current buggy method. pay attention to static methods,
  because they allow to access fields that are static only
  [Done. See visibleVarsCollector -- static vs non-static fields]
* static field cannot be referenced with this expression. For example, this.field
  [Done, see assignment test. E.g., private static int fieldHere => referenced by Assignment.fieldHere]

* handle the loading of dumped expected output
 [Done. See RunTime expectedOutput]

* add localization of more kind of expressions in main.scala.frontend. See FrontEndInstrumentVisitor,
  we now only support assignments, if statement conditions, for-loop conditions, while conditions, guards
  [Done according to angelix default]

* check constant when instrumenting. E.g., would it be ok? to instrument the constant 15 in the following example:
  angelixChoose like x = angelixChooseInt(15, ...)
  [Done, we can repair error that involve RHS as constant. See Field test]

* do not make array access or array to be symbolic. This is not properly handled yet.

* allow defect class option, only instrument expressions belonging to specified defect classes
  [Done]

* Handle guards defect class
  [Done]
   Do as follows:
     - frontend: check if current node is a statement and guards options is in defect classes. if so, then add trace before that statement.
     - backend: check if chosen fault E is a statement. If so, add guard if(angelixChoose(..., true)) E
     - apply patch: check if the location being replaced is a statement E. If so, it means we change the guard as: if(synthesized patch) E

* Handle possibly uninitialized variables when instrumenting angelixChoose. Do not add uninitialized variables
  to environment variables when instrumenting angelixChoose for symbolic execution.

* Handle double, string

* instrument backend may give error: missing return statement

* Allow golden version instead of the need of assert file every time
  [done]
* Handle largest possible expression that we can instrument,
  e.g., we do/should not instrument function call in an expression
  [done, see code in frontend and frontend test]

* when angelic value = original satisfies the whole pc, just prefer the original as the value for angelic value
e.g., int_choice_68_21_68_31_0_angelic[-10000000] <= CONST_3 &&
      int_choice_68_21_68_31_0_original[1] == CONST_1 && ...

* collect used method call. See bug fix for Math version 33 in Defects4j

* instrument trace for not. e.g., !(x>1), currently !(trace_boolean(x>1)) ==> should be trace_boolean(!(x>1))
