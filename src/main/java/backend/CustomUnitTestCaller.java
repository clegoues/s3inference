package backend;

import gov.nasa.jpf.symbc.Debug;

import java.io.FileReader;
import java.io.*;
/**
 * Created by dxble on 9/1/16.
 */
public class CustomUnitTestCaller {
    private static String pc = "";
    private static int maxIndex = 0;

    public static void main(String[] argv) throws Throwable {//Exception
        System.out.println("Calling CustomUnitTestCaller to write PC: "+argv[0]);
        //System.out.println("******START*******");
        Boolean debug = Boolean.parseBoolean(argv[1]);
        printDebugInfo("***SOLVED PC***",debug);
        //System.out.println("Solved PC is "+Debug.getSolvedPC());
        try {
            pc = readPC(argv[0]);
            printDebugInfo("Read PC: \n"+pc, debug);
            String nextPC = Debug.getSolvedPC();
            if(pc.compareTo("") == 0) {
                pc = nextPC + "\n" + pc;
            }else{
                String pcConstraintIndex = pc.split(System.getProperty("line.separator"))[0];
                String nextPCConstraintIndex = nextPC.split(System.getProperty("line.separator"))[0];
                int indexNextPC = Integer.valueOf(nextPCConstraintIndex.split("=")[1].trim());
                printDebugInfo("read: "+pcConstraintIndex+" next: "+nextPCConstraintIndex, debug);
                if(indexNextPC>maxIndex) {
                    pc = nextPC + "\n" + pc;
                }
                else{
                    //String index = pcConstraintIndex.split("=")[1].trim();
                    //int nextIndex = Integer.valueOf(index) + 1;
                    int nextIndex = maxIndex+1;
                    printDebugInfo("max index "+maxIndex+" next "+nextIndex, debug);
                    maxIndex=nextIndex;
                    nextPC = nextPC.replace(nextPCConstraintIndex, "constraint # = "+nextIndex);
                    pc = nextPC + "\n" + pc;
                }
            }
            writeSolvedPC(argv[0], pc);
            printDebugInfo("Wrote PC: "+pc, debug);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        printDebugInfo("***END SOLVED PC***", debug);
    }

    public static void printDebugInfo(String content, Boolean debug){
        if (debug)
            System.out.println(content);
    }

    public static void writeSolvedPC(String fileName, String solvedPC) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(solvedPC);
        bw.close();
    }

    public static String readPC(String fileName){
        BufferedReader br = null;
        String fileContent = "";
        try {

            String sCurrentLine;
            br = new BufferedReader(new FileReader(fileName));
            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                if(sCurrentLine.contains("constraint # = ")) {
                    int index = Integer.valueOf(sCurrentLine.split("=")[1].trim());
                    if(index>maxIndex)
                        maxIndex=index;
                }
                fileContent += sCurrentLine+"\n";
            }

            //System.out.println("Reading PC: "+fileContent);
            return fileContent;
        } catch (IOException e) {
            //e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                //ex.printStackTrace();
            }finally {
                return fileContent;
            }
        }
    }
}
