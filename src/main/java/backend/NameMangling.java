package backend;

import org.eclipse.jdt.core.dom.InfixExpression;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dxble on 8/30/16.
 */
public class NameMangling {

    public static void main(String[] args){
        //Matcher test = nullCheckPatternNEQ.matcher("NCK_NEQ_100");
        //while (test.find()){
        //    System.out.println(test.group(0));
        //}

        Matcher test2 = mangledVarPattern.matcher("ENCODEVAR_UsedVar_100");
        while (test2.find()){
            System.out.println(test2.group(0));
        }
    }

    public static HashMap<String, String> mangledMethodCallKeyName = new HashMap<>();
    public static HashMap<String, String> mangledMethodCallNameKey = new HashMap<>();
    public static Pattern methodCallPattern = Pattern.compile("MC_\\d+");

    public static HashMap<String, String> mangledNullCheckKeyName = new HashMap<>();
    public static HashMap<String, String> mangledNullCheckNameKey = new HashMap<>();
    public static Pattern nullCheckPatternEQ = Pattern.compile("NCK_EQ_\\d+");
    public static Pattern nullCheckPatternNEQ = Pattern.compile("NCK_NEQ_\\d+");

    public static HashMap<String, String> mangledVarKeyName = new HashMap<>();
    public static HashMap<String, String> mangledVarNameKey = new HashMap<>();
    public static Pattern mangledVarPattern = Pattern.compile("ENCODEVAR_[a-zA-Z]+_\\d+");

    private static Random rand = new Random(0);

    public static String mangleVariable(String name, String unique){
        //return name+"_"+unique;
        /*String mangled = mangledVarNameKey.get(name);
        if(mangled == null){
            String hc = Integer.toString(rand.nextInt(1000000));
            String key = unique+"_"+hc;
            mangledVarKeyName.put(key, name);
            mangledVarNameKey.put(name, key);
            return key;
        } else return mangled;*/
        return name;

    }

    public static void readMangledVarFromFile(File file) {
        try {
            String[] ss = mylib.Lib.readFile2Lines(file);
            for(String s: ss){
                String[] sp = s.split(" ");
                mangledVarKeyName.put(sp[0], sp[1]);
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    public static void readMangledMethodCallFromFile(File file) {
        try {
            String[] ss = mylib.Lib.readFile2Lines(file);
            for(String s: ss){
                String[] sp = s.split(" ");
                mangledMethodCallKeyName.put(sp[0], sp[1]);
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    public static void readMangledNullCheckFromFile(File file) {
        try {
            String[] ss = mylib.Lib.readFile2Lines(file);
            for(String s: ss){
                String[] sp = s.split(" ");
                mangledNullCheckKeyName.put(sp[0], sp[1]);
            }
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    public static void dumpMangledMethodCall(File file) {
        dumpMangled(file, mangledMethodCallKeyName);
    }

    public static void dumpMangledNullCheck(File file) {
        dumpMangled(file, mangledNullCheckKeyName);
    }

    public static void dumpMangledVar(File file) {
        dumpMangled(file, mangledVarKeyName);
    }

    private static void dumpMangled(File file, HashMap<String, String> mangled) {
        String s = "";
        for(Map.Entry<String, String> entry: mangled.entrySet()){
            s += entry.getKey()+" "+entry.getValue()+"\n";
        }
        try {
            mylib.Lib.writeText2File(s, file);
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    public static String mangleMethodCall(String name) {
        //return "_M_"+name+"_C_";
        String mangled = mangledMethodCallNameKey.get(name);
        if(mangled == null){
            String hc = Integer.toString(rand.nextInt(1000000));
            String key = "MC_"+hc;
            mangledMethodCallKeyName.put(key, name);
            mangledMethodCallNameKey.put(name, key);
            return key;
        } else return mangled;
    }

    public static String mangleNullCheck(String name, InfixExpression.Operator eqOrNeq) {
        //return "_M_"+name+"_C_";
        String mangled = mangledNullCheckNameKey.get(name);
        if(mangled == null){
            String hc = Integer.toString(rand.nextInt(1000000));
            String op = eqOrNeq == InfixExpression.Operator.EQUALS? "EQ" : "NEQ";
            String key = "NCK_"+op+"_"+hc;
            mangledNullCheckKeyName.put(key, name);
            mangledNullCheckNameKey.put(name, key);
            return key;
        } else return mangled;
    }

    public static String unmangleVariable(String var){
        String[] sp = var.split("_");
        int i;
        String res = "";
        for(i = 0; i < sp.length - 1; i++){
            res += sp[i];
        }
        return res;
    }
}
