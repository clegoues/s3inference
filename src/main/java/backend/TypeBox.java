package backend;


/**
 * Created by dxble on 8/27/16.
 */
public class TypeBox {
    private String stringVal = null;// not null

    private boolean objectNullAccess(){
        return false ; //stringVal.compareTo("OBJECT_IS_NULL") == 0;
    }

    private boolean booleanVal;
    private double doubleVal;
    private int intVal;
    private float floatVal;
    private char charVal;

    //private String internalAccess = null;

    private Type type;

    public enum Type{
        STRING_TYPE, INT_TYPE, BOOL_TYPE, DOUBLE_TYPE, FLOAT_TYPE, CHAR_TYPE
    }

    public TypeBox(int x, Type t){
        intVal = x;
        type = t;
    }
    public TypeBox(double x, Type t){
        doubleVal = x;
        type = t;
    }
    public TypeBox(boolean x, Type t){
        booleanVal = x;
        type = t;
    }

    // for internal access type and String type
    public TypeBox(String x, Type t){
        if(x.compareTo("OBJECT_IS_NULL") != 0){
            // This is for field and method call accesses
            // e.g., object X.methodA(), we encode return value of X.methodA() as a string

            if(t == Type.BOOL_TYPE){
                booleanVal=Boolean.getBoolean(x);
            }else if(t == Type.INT_TYPE){
                intVal = Integer.parseInt(x);
            }else if(t == Type.DOUBLE_TYPE){
                doubleVal = Double.parseDouble(x);
            }else if(t == Type.FLOAT_TYPE){
                floatVal = Float.parseFloat(x);
            }else if(t == Type.CHAR_TYPE){
                charVal = x.charAt(0);
            }else{
                stringVal = x;
                type = t;
            }
        }else {
            stringVal = null;
            type = t;
        }
    }

    public TypeBox(float x, Type t){
        floatVal = x;
        type = t;
    }

    public TypeBox(char x, Type t){
        charVal = x;
        type = t;
    }

    public boolean isBoolType(){
        return type == Type.BOOL_TYPE /*&& !objectNullAccess()*/;
    }

    public boolean isIntType(){
        return type == Type.INT_TYPE /*&& !objectNullAccess()*/;
    }

    public boolean isDoubleType(){
        return type == Type.DOUBLE_TYPE /*&& !objectNullAccess()*/;
    }

    public boolean isStringType(){
        return type == Type.STRING_TYPE && stringVal != null /*&& !objectNullAccess()*/;
    }

    public boolean isFloatType(){
        return type == Type.FLOAT_TYPE /*&& !objectNullAccess()*/;
    }

    public boolean isCharType(){
        return type == Type.CHAR_TYPE /*&& !objectNullAccess()*/;
    }

    public boolean getBooleanVal() {
        return booleanVal ;
    }

    public double getDoubleVal() {
        return doubleVal;
    }

    public int getIntVal() {
        return intVal;
    }

    public String getStringVal() {
        return stringVal;
    }

    public float getFloatVal(){return floatVal;}

    public char getCharVal(){return charVal;}

    public Type getType(){return type;}
}
