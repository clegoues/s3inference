package junithandler;


import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dxble on 2/11/17.
 */
public class SingleTestNGTestRunner {
    // -Xbootclasspath/a: to add classpath to debug with IDE
    public static String SUCCESS = "true";
    public static String FAILURE = "false";
    public static String RESULT_PREFIX_PRINT = "HISTORICALFIX EVALUATION RESULT";
    public static String SEPARATOR = "=";

    public static class TestListener extends TestListenerAdapter {

        private int failureCount = 0;

        public int getFailureCount(){
            return failureCount;

        }

        @Override
        public void onTestFailure(ITestResult result) {
            // do what you want to do
            System.out.println("F");
            failureCount += 1;
        }

        @Override
        public void onTestSuccess(ITestResult result) {
            // do what you want to do
            System.out.println("S");
        }

        @Override
        public void onTestSkipped(ITestResult result) {
            // do what you want to do
            System.out.println("SK");
        }
    }

    public static void main(String... args) throws ClassNotFoundException {
        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");

        XmlTest test = new XmlTest(suite);
        test.setName("TmpTest");
        List<XmlClass> classes = new ArrayList<XmlClass>();
        String[] classMethodNames = args[0].split("#");

        XmlClass x = new XmlClass(classMethodNames[0]);
        if(classMethodNames.length == 2) {
            // testng 6.8.8
            List<XmlInclude> methods = new ArrayList<XmlInclude>();
            XmlInclude m = new XmlInclude(classMethodNames[1]);
            methods.add(m);
            x.setIncludedMethods(methods);

            // testng 15
            //List<String> methodStrings = new ArrayList<>();
            //methodStrings.add(classMethodNames[1]);
            //x.setIncludedMethods(methodStrings);

        }
        classes.add(x);
        test.setXmlClasses(classes) ;

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        TestNG testng = new TestNG();
        testng.setXmlSuites(suites);

        TestListener listener = new TestListener();
        //TestListenerAdapter listener = new TestListenerAdapter();
        testng.addListener(listener);
        testng.setVerbose(10);
        testng.run();
        /*TestNG testng = new TestNG();
        List<String> testNames = new ArrayList<String>();
        testNames.add(args[0]);
        testng.setTestNames(testNames);
        //testng.setTestClasses(new Class[] { FilesystemBlobStoreTest.class });
        testng.run();*/
        System.out.println(RESULT_PREFIX_PRINT+SEPARATOR+(listener.getFailureCount() == 0));
        System.exit(listener.getFailureCount() == 0 ? 0 : 1);
        //System.out.println(RESULT_PREFIX_PRINT+SEPARATOR+(listener.getFailedTests().size()==0));
        //System.exit(listener.getFailedTests().size() == 0 ? 0 : 1);
    }
}
