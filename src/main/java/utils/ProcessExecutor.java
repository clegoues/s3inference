package utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by dxble on 10/7/16.
 */
public abstract class ProcessExecutor {
    private Logger logger = Logger.getLogger(this.getClass());



    public int executeCommandLine(final String commandLine,
                                         final boolean printOutput,
                                         final boolean printError,
                                         final int timeout)
            throws IOException, InterruptedException, TimeoutException
    {
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec(commandLine);

        Worker worker = new Worker(process, timeout);
        worker.start();
        try {
            worker.join(timeout);
            if (worker.exit != null)
                return worker.exit;
            else
                throw new TimeoutException();
        } catch(InterruptedException ex) {
            worker.interrupt();
            Thread.currentThread().interrupt();
            throw ex;
        } finally {
            process.destroy();
        }
    }

    public boolean execute(String path, ArrayList<String> params, int waitTime) {
        Process p = null;


        //if(!ProjectConfiguration.validJDK())
        //	throw new IllegalArgumentException("jdk folder not found, please configure property jvm4testexecution in the configuration.properties file");

        String javaPath = System.getProperty("java.home");
        javaPath += File.separator + "bin/java";
        String systemcp = System.getProperty("java.class.path");

        // Be careful when rewrite path: the ones that come first would get picked first,
        // and the picked ones would not get overwritten!
        String fullPath =  path + File.pathSeparator + systemcp;

        logger.debug("Classpath: "+fullPath);
        try {

            List<String> command = new ArrayList<String>();
            command.add(javaPath);
            command.add("-cp");
            command.add(fullPath);
            command.addAll(params);
            ProcessBuilder pb = new ProcessBuilder(command.toArray(new String[command.size()]));
            pb.redirectOutput();
            pb.redirectErrorStream(true);
            long t_start = System.currentTimeMillis();
            p = pb.start();
            String cm2 = command.toString().replace("[", "").replace("]", "").replace(",", " ");
            //log40.debug("Executing process: \n"+cm2);

            Worker worker = new Worker(p, waitTime);
            worker.start();
            worker.join(waitTime);
            long t_end = System.currentTimeMillis();
            if(worker.exit != null) {
            /*if(!p.waitFor(waitTime, TimeUnit.SECONDS)){
                logger.debug("TIMEOUT");
                getResult(p);
                p.destroy();
                worker.exit
                //return false;
            }*/
                //worker.interrupt();
                // ---
                int exitvalue = p.exitValue();
                boolean success = getResult(p);
                p.destroy();
                logger.debug("Execution time " + ((t_end - t_start) / 1000) + " seconds");
                return success;
            }else return false;

        } catch (IllegalArgumentException|IOException | InterruptedException ex) {
            System.out.println("The thread continues working " + ex.getMessage());
            if (p != null)
                p.destroy();
            throw new RuntimeException("Process return null");
        }

    }

    abstract protected boolean getResult(Process p);

    private static class Worker extends Thread {
        private final Process process;
        private Integer exit;
        private int wait;
        private Worker(Process process, int waitTime) {
            this.process = process;
            wait=waitTime;
        }

        public void run() {
            try {
                exit = process.waitFor();
            } catch (InterruptedException ignore) {
                return;
            }
        }
    }

}
