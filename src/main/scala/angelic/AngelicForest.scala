package angelic

import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 9/8/16.
  */
// AngelicFix definitions

object AngelicFix {

  sealed trait VariableValue
  case class IntVal(name: String, value: Int) extends VariableValue
  case class BoolVal(name: String, value: Boolean) extends VariableValue
  case class CharVal(name: String, value: Char) extends VariableValue
  case class DoubleVal(name: String, value: Double) extends VariableValue
  case class FloatVal(name: String, value: Float) extends VariableValue
  case class StringVal(name: String, value: String) extends VariableValue

  def renameVal(v: VariableValue, newname: String): VariableValue = {
    v match {
      case IntVal(n, v)  => IntVal(newname, v)
      case BoolVal(n, v) => BoolVal(newname, v)
      case CharVal(n, v) => CharVal(newname, v)
      case DoubleVal(n, v) => DoubleVal(newname, v)
      case FloatVal(n, v) => FloatVal(newname, v)
      case StringVal(n, v) => StringVal(newname, v)
    }
  }

  def getName(v: VariableValue): String = {
    v match {
      case IntVal(n, v)  => n
      case BoolVal(n, v) => n
      case CharVal(n, v) => n
      case DoubleVal(n, v) => n
      case FloatVal(n, v) => n
      case StringVal(n, v) => n
    }
  }

  case class AngelicValue(val context: ArrayBuffer[VariableValue], var value: VariableValue, expression: String, instId: Int){
    def appendContext(v: VariableValue): Unit ={
      context.append(v)
    }
    def setAngelicValue(av: VariableValue) : Unit = {
      value = av
    }

    /*override def toString(): String = {
      val contextString = context.foldLeft("context: "){(res, v) => res+" "+v}
      "AngelicValue("+contextString+" * angelic value: "+value+" * exp: "+expression+" * instID: "+instId
    }*/
  }

  type AngelicPath = List[AngelicValue]

  type AngelicForest = Map[String, List[AngelicPath]]

}

