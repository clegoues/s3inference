package backend

import java.io.{File, OutputStream, PrintStream}

import angelic.AngelicFix.AngelicValue
import driver.Options
import org.apache.log4j.Logger
import testrunner.TestCase
import utils.FileFolderUtils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 10/4/16.
  */
object Inference {
  val logger = Logger.getLogger(this.getClass)
  var originalSystemOut: PrintStream = null

  def hideSystemOut(): PrintStream = {
    val orig = System.out
    val dummyStream  = new PrintStream(new OutputStream(){
      override def write(b: Int) {
        //NO-OP
      }
    })

    System.setOut(dummyStream)
    return orig
  }

  def unHideSystemOut(orig: PrintStream): Unit = {
    System.setOut(orig)
  }

  def inferSpec(reducedNegTests: ArrayBuffer[TestCase[Any]], reducedPosTests: ArrayBuffer[TestCase[Any]], symbcLocs: java.util.ArrayList[String], angelixFolder: String) = {
    // Running tests for specification inference.
    // angelicForest is a map of test id -> angelic paths
    val angelicForest = new mutable.HashMap[String, ArrayBuffer[Iterable[AngelicValue]]]()

    def inferHelper(reducedTestSuite: ArrayBuffer[TestCase[Any]]) = {
      reducedTestSuite.foreach(test => {
        val jpfRunner = new JpfRunner()
        logger.info("Running JPF for specification inference")
        if (!Options.debug) {
          originalSystemOut = hideSystemOut()
        }
        jpfRunner.run(test, symbcLocs)
        if (!Options.debug)
          unHideSystemOut(originalSystemOut)

        logger.info("Finished running JPF with test: " + test)
        // At this step, we have solvedPC file already parsed, and stored in jpfRunner.solvedPC
        if (jpfRunner.solvedPC != null) {
          val paths = jpfRunner.solvedPC.getAngelicPaths()
          if (paths != null && paths.size > 0) {
            angelicForest += test.getId().toString -> paths
            if(!new File(angelixFolder+File.separator+driver.S3Properties.ACTUAL_VAR_TYPES_FILE).exists())
              FileFolderUtils.write2File(angelixFolder+File.separator+driver.S3Properties.ACTUAL_VAR_TYPES_FILE, jpfRunner.solvedPC.variableTypes.mkString("\n").replace(" -> ",","))
            logger.info("Test: "+test.getId()+" Added angelic path: "+paths)
          }
        }
      })
    }

    // Now perform inference on negative tests first.
    // If inferred angelic forest has size > 0, continue to infer spec for positive tests/
    // Otherwise, proceed and return an empty forest.
    // if infer no specs for negative tests, just ignore this fault and proceed to next fault immediately
    // This is because specs for negative tests are the most important one!
    inferHelper(reducedNegTests)
    if(angelicForest.size > 0)
      inferHelper(reducedPosTests)

    angelicForest
  }
}
