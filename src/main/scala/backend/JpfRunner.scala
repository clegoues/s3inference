package backend

import java.util

import driver.{Options, S3Properties}
import gov.nasa.jpf.{Config, JPF, JPFConfigException, JPFException}
import gov.nasa.jpf.symbc.SymbolicListener
import testrunner.TestCase
import utils.FileFolderUtils
import java.io.{File, IOException}

import gov.nasa.jpf.symbc.numeric.{PCChoiceGenerator, PathCondition}
import gov.nasa.jpf.vm.{ChoiceGenerator, JVMForwarder, VM}
import org.apache.log4j.Logger

/**
  * Created by dxble on 9/2/16.
  */
class JpfRunner() {
  val logger = Logger.getLogger(this.getClass)
  var commonJpf: JPF = null
  var solvedPC: ParseSolvedPC = null

  def deleteDriver(): Unit ={
    val dir = FileFolderUtils.getInstrumentedDirWithPrefix()
    val found = new util.ArrayList[File]()
    mylib.Lib.walk(dir, "CustomUnitTestCaller", found)
    if(found.size()>0){
      found.get(0).delete()
    }
  }

  def deletePCFile(): Unit ={
    val pcFile = FileFolderUtils.getInstrumentedDirWithPrefix() + File.separator + "solvedPC.txt"
    if(new File(pcFile).exists())
      new File(pcFile).delete()
  }

  def commonSPFConfig(test: TestCase[Any], symbcLocs: java.util.ArrayList[String]): Unit = {
    if(commonJpf != null)
      return
    logger.info("Creating test driver for running with JPF for test: "+test)
    val driverCreator = new UnitTestDriver
    driverCreator.createTestDriver(test)
    driverCreator.compile()

    // this initializes the JPF configuration from default.properties, site.properties
    // configured extensions (jpf.properties), current directory (jpf.properies) and
    // command line args ("+<key>=<value>" options and *.jpf)
    val conf = JPF.createConfig(Array[String]())

    // ... modify config according to your needs
    import scala.collection.JavaConversions._
    val instrFolder = FileFolderUtils.getInstrumentedDirWithPrefix()
    val deps = Options.getDependenciesList().foldLeft(instrFolder){ (res, dep) => res+";"+dep}

    logger.debug("JPF classpath: "+deps)
    //conf.setProperty("@using","jpf-nhandler")
    //conf.setProperty("@using","jpf-symbc")
    conf.setProperty("classpath", deps)
    conf.setProperty("native_classpath", deps)

    //conf.setProperty("vm.reuse_tid","true")
    //conf.setProperty("nhandler.resetVMState","false")
    //conf.setProperty("log.level","finest") // @See http://babelfish.arc.nasa.gov/trac/jpf/wiki/user/output
    //Delegates are separated by ,
    if(Options.nhandlerDelegates != null)
      conf.setProperty("nhandler.spec.delegate", Options.nhandlerDelegates)

    //conf.setProperty("nhandler.spec.skip", "java.util.regex.Matcher.end")
    conf.setProperty("nhandler.delegateUnhandledNative", Options.nhandlerDelegateUnhandledNative.toString)
    //conf.setProperty("nhandler.genSource","true")
    //conf.setProperty("nhandler.clean","false")

    conf.setProperty("vm.classpath", deps)
    conf.setProperty("sourcepath", Options.angelixRoot)
    conf.setProperty("search.depth_limit", Options.jpfSearchDepth)
    //conf.setProperty("search.multiple_errors", Options.jpfMultipleErrors.toString())
    conf.setProperty("target.args", instrFolder+File.separator+"solvedPC.txt"+","+Options.debug)

    // This is somewhat important. If the value lies below or over this thresholds, silent errors may happen...
    conf.setProperty("symbolic.min_double",Options.jpfMinDouble.toString)
    conf.setProperty("symbolic.max_double", Options.jpfMaxDouble.toString)
    conf.setProperty("symbolic.min_int",Options.jpfMinInt.toString)
    conf.setProperty("symbolic.max_int", Options.jpfMaxInt.toString)
    conf.setProperty("search.multiple_errors", Options.jpfSearchMultipleErrors.toString)

    conf.setProperty("symbolic.string_dp", "automata")
    // Set if we should print output of the SUT
    conf.setProperty("vm.tree_output", Options.debug.toString)
    conf.setProperty("symbolic.dp",Options.jpfSymbcSolver)
    conf.setProperty("symbolic.string_dp","automata")

    val target = if(driverCreator.driverPackage == null) "CustomUnitTestCaller" else driverCreator.driverPackage+".CustomUnitTestCaller"
    conf.setProperty("target", target)
    val jpf = new JPF(conf)

    // ... explicitly create listeners (could be reused over multiple JPF runs)
    val myListener: MySymbolicListener = new MySymbolicListener(conf, jpf, symbcLocs)
    // ... set your listeners
    jpf.addListener(myListener)
    commonJpf = jpf
  }

  def run(test: TestCase[Any], symbcLocs: java.util.ArrayList[String]): Unit ={
    try {

      commonSPFConfig(test, symbcLocs)

      System.setProperty(S3Properties.RUNNING_TEST_NUMBER, test.getId().toString)
      System.setProperty(S3Properties.DEBUG, Options.debug.toString)
      // Bachle: This tells jpf to get properties from the host VM
      // We have some properties that are passed through our RunTime.java during symbolic execution
      // Thus, setting this option is crucial. @See package backend RunTime.java
      commonJpf.getConfig.setProperty("vm.sysprop.source", "host")

      commonJpf.run()
      solvedPC = new ParseSolvedPC(false)

      if (commonJpf.foundErrors()){
        // ... process property violations discovered by JPF
      }
    } catch{
      case e: JPFConfigException =>{
        logger.debug("JPF config exception: "+e.getMessage)
        //solvedPC = new ParseSolvedPC(true)
      }
      // ... handle configuration exception
      // ...  can happen before running JPF and indicates inconsistent configuration data

      case e: JPFException => {
        //logger.info("JPF exception: "+e.getMessage)
        e.printStackTrace()
        //solvedPC = new ParseSolvedPC(true)
        val search = commonJpf.getSearch
        val vm: VM = search.getVM
        val cg: ChoiceGenerator[_] = MySymbolicListener.findPCChoiceGenerator(vm)
        if ((cg.isInstanceOf[PCChoiceGenerator]) && (cg.asInstanceOf[PCChoiceGenerator]).getCurrentPC != null) {
          val pc: PathCondition = (cg.asInstanceOf[PCChoiceGenerator]).getCurrentPC
          pc.solve()
          val fileName: String = FileFolderUtils.getInstrumentedDirWithPrefix + File.separator + "solvedPC.txt"
          if (!new File(fileName).exists) {
            try {
              val parser = new ParseSolvedPC(false)
              val removedLastInstance = parser.removeLastInstance(pc.toString.split(System.getProperty("line.separator")))
              if (removedLastInstance != null) {
                CustomUnitTestCaller.writeSolvedPC(fileName, removedLastInstance)
                solvedPC = new ParseSolvedPC(false)
                logger.info("Wrote PC when search finished with error by JPF! Popped last pc instance!")
              }
            }
            catch {
              case e: IOException => {
                e.printStackTrace()
              }
            }
          }
        }
      }

      case e: TestDriverNotCompileException => {
        logger.info("Run JPF with exception: " +e.getMessage)
      }

      // ... handle exception while executing JPF, can be further differentiated into
      // ...  JPFListenerException - occurred from within configured listener
      // ...  JPFNativePeerException - occurred from within MJI method/native peer
      // ...  all others indicate JPF internal errors
      case e: RuntimeException => {
        // Note that: JPF may throw run time exception, for example, not handle symbolic index for arrays.
        logger.info("Runtime Exception while running JPF: "+e.getMessage)
        logger.info("Original exception: "+e)
        e.printStackTrace()
        println(e.getLocalizedMessage)
      }

    }finally {
      deleteDriver()
      deletePCFile()
    }
  }
}