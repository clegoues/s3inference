package backend

import java.io.File

import angelic.AngelicFix._
import testrunner.TestCase
import utils.FileFolderUtils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import net.liftweb.json._
import net.liftweb.json.Serialization.write

/**
  * Created by dxble on 9/7/16.
  */
class ParseSolvedPC{

  // Map [VariableName -> Value]
  type SolvedPath = mutable.HashMap[String, String]

  // Map [exp-instance, AngelicValue]
  type PathContent =  mutable.HashMap[String, AngelicValue]

  var angelicPaths: ArrayBuffer[Iterable[AngelicValue]] = null
  var popLastInstance: Boolean = false
  val variableTypes: mutable.HashMap[String, String] = new mutable.HashMap[String, String]()

  def this(toPopLastInstance: Boolean){
    this()
    popLastInstance = toPopLastInstance
    getAngelicPaths()
  }

  def removeLastInstance(pc: Array[String]): String ={
    val lastInstance = pc collectFirst {case l if l.contains("choice") => l.split(" ").filter(_.contains("choice")).head.split(RunTime.VAR_SEPARATOR)(6)}
    val removedLastInstance = pc.foldLeft(""){
      (res, line) => {
        if(line.contains("[") && line.contains("]") && line.contains("choice")){
          val choiceVar = line.trim().split(" ").filter(p=>p.contains("choice"))
          if(choiceVar != null && choiceVar.length > 0) {
            val containsLastInstance = choiceVar.exists(v => {
              val variableNameValue = v.split("\\[")
              // replace the ( and ) with nothing, to avoid the case x = (y + const)
              val variableName = variableNameValue(0).replace("(","").replace(")","")
              // Compare the instance of the choice var.
              variableName.split(RunTime.VAR_SEPARATOR)(6).compareTo(lastInstance.getOrElse(throw new RuntimeException("We do not find any latest instance"))) == 0
            })
            if(!containsLastInstance){
              res+line+"\n"
            }
            else res
          }else res
        }else{
          res+line+"\n"
        }
      }
    }
    if(!removedLastInstance.contains("choice"))
      return null
    else return removedLastInstance
  }

  // ArrayBuffer of Paths, each path is indicated by starting by constraint # = number
  private def parse(): mutable.HashMap[String, SolvedPath] = {
    val pcFile = FileFolderUtils.getInstrumentedDirWithPrefix()+ File.separator + "solvedPC.txt"
    if(!new File(pcFile).exists())
      return null

    val lines = mylib.Lib.readFile2Lines(new File(pcFile))
    val lastInstance = lines collectFirst {case l if l.contains("choice") => l.split(RunTime.VAR_SEPARATOR)(6)}
    val allPaths = new mutable.HashMap[String,SolvedPath]
    var currentPathIndex: String = null
    lines.foreach{
      line => {
        if(line.contains("constraint")){
          // TODO: is this line of code needed?
          //allPaths += line -> (new SolvedPath)
          currentPathIndex = line
        }
        val path = allPaths.getOrElse(currentPathIndex, new SolvedPath)

        if(line.contains("[") && line.contains("]")){
          //println(line.trim().split("=="))
          val choiceVar = line.trim().split(" ").filter(p=>p.contains("choice"))
          if(choiceVar != null && choiceVar.length > 0) {
            choiceVar.foreach(v => {
              val variableNameValue = v.split("\\[")
              // replace the ( and ) with nothing, to avoid the case x = (y + const)
              val variableName = variableNameValue(0).replace("(","").replace(")","")
              val variableValue = variableNameValue(1).replace("]", "")
              // Compare the instance of the choice var.
              // If encountered exception, we do note care about the latest instance
              if(popLastInstance){
                if(variableName.split(RunTime.VAR_SEPARATOR)(6).compareTo(lastInstance.getOrElse(throw new RuntimeException("We do not find any latest instance"))) != 0) {
                  path += variableName -> variableValue
                  allPaths.update(currentPathIndex, path)
                }
              }else{
                path += variableName -> variableValue
                allPaths.update(currentPathIndex, path)
              }
            })
          }
        }
      }
    }
    allPaths
  }

  private def parseVariableValue(typeS: String, name: String, value: String, exp: String): VariableValue = {
    if(typeS.compareTo("int") == 0){
      variableTypes += exp+"-"+name -> "int"
      new IntVal(name, value.trim().toInt)
    }else if(typeS.compareTo("bool") == 0){
      variableTypes += exp+"-"+name -> "bool"
      val intValue = value.trim().toInt
      if(intValue == 0)
        new BoolVal(name, false)
      else
        new BoolVal(name, true)
    }else if(typeS.compareTo("char") == 0){
      variableTypes += exp+"-"+name -> "char"
      //new CharVal(name, value.trim().toInt.toChar)
      // Currently char is cast to int
      new IntVal(name, value.trim().toInt)
    }else if(typeS.compareTo("double") == 0){
      variableTypes += exp+"-"+name -> "double"
      // For now we only handle double value that can be converted to int.
      // For example, 3.0 => 3; 3.5 => then could be error
      new IntVal(name, value.trim().toDouble.toInt)
    }else if(typeS.compareTo("float") == 0){
      variableTypes += exp+"-"+name -> "float"
      // For now we only handle double value that can be converted to int.
      // For example, 3.0 => 3; 3.5 => then could be error
      new IntVal(name, value.trim().toFloat.toDouble.toInt)
    }
    else {
      throw new RuntimeException("Not yet handled type: " + typeS)
    }
  }

  private def solvedPaths2AngelicPaths(): ArrayBuffer[Iterable[AngelicValue]] = {
    val parsedResult = parse()
    if(parsedResult == null)
      return null

    val solvedPaths = parsedResult.values
    val allPaths = new ArrayBuffer[Iterable[AngelicValue]]()

    solvedPaths.foreach(path => {
      // Map [exp-instance, AngelicValue]
      val pathContent = new PathContent
      var seenAngelic = false
      path.foreach(variable => {
        val name = variable._1
        if(name.contains("choice")) {
          val value = variable._2
          val sp = name.split(RunTime.VAR_SEPARATOR)
          val exp = sp(2)+"-"+sp(3)+"-"+sp(4)+"-"+sp(5)
          val instance = sp(6)
          val av = pathContent.getOrElse(exp+"-"+instance, AngelicValue(new ArrayBuffer[VariableValue](), null, exp, instance.toInt))
          if(sp(7).compareTo("angelic") == 0){
            val variableValue = parseVariableValue(sp(0), "angelic", value, exp)
            av.setAngelicValue(variableValue)
            seenAngelic = true
          }else{
            // This is context
            val variableName = sp.takeRight(sp.length-8).mkString("_")
            val variableValue = parseVariableValue(sp(0), variableName, value, exp)
            av.appendContext(variableValue)
          }
          pathContent.update(exp+"-"+instance, av)
        }
      })
      if(seenAngelic) {
        // Filter out those with null angelic value
        allPaths.append(pathContent.values.filter(v => v.value != null))
      }
    })

    allPaths
  }

  def getAngelicPaths(): ArrayBuffer[Iterable[AngelicValue]] = {
    if(angelicPaths == null){
      angelicPaths = solvedPaths2AngelicPaths()
    }

    return angelicPaths
  }

  def solvedPC2Json(): String ={
    val allAngelicPaths = solvedPaths2AngelicPaths()
    implicit val formats = DefaultFormats
    val jsonString = write(allAngelicPaths)
    println(jsonString)
    jsonString
    /*allAngelicPaths.foreach(p => {
      implicit val formats = DefaultFormats
      val jsonString = write(p)
      println(jsonString)
    })*/
  }
}
object ParseSolvedPC{
  def main(argv: Array[String]): Unit ={
    System.setProperty("ANGELIX_FOLDER", "/home/dxble/workspace/jangelix/angelix")
    val parser = new ParseSolvedPC(false)
    val lines = mylib.Lib.readFile2Lines(new File("/home/dxble/workspace/jangelix/angelix/instrumented/solvedPC.txt"))
    val rem = parser.removeLastInstance(lines)
    println(rem)
    //println(parser.parse())
    //parser.solvedPaths2AngelicPaths()
    //parser.solvedPC2Json()
  }
}
