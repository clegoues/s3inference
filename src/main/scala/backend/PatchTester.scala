package backend

import java.io.File
import java.net.URL
import java.util.regex.Pattern

import driver.S3Properties
import localization.FaultIdentifier
import org.apache.log4j.Logger
import parser.javaparser.JavaParser
import utils.ASTUtils

/**
  * Created by dxble on 9/9/16.
  */
object PatchTester {
  val logger = Logger.getLogger(this.getClass)
  def main(args: Array[String]) {
    val str = "1-20-101-30"
    val str2 = "a > b"
    val str3 = "MC_123123 + x + MC_1435435"
    val MY_PATTERN = Pattern.compile("MC_\\d+")
    println(str.matches("\\d+-\\d+-\\d+-\\d+"))
    println(str2.matches("\\d+-\\d+-\\d+-\\d+"))
    val m = MY_PATTERN.matcher(str3)
    while (m.find()) {
      val s = m.group(0)
      // s now contains "BAR"
      println(s)
    }
  }

  def readMangledMethodCall() = {
    val aglFolder = System.getProperty(S3Properties.ANGELIX_FOLDER)
    val mangledMCFile = aglFolder + File.separator + S3Properties.MANGLED_METHOD_CALL_FILE
    NameMangling.readMangledMethodCallFromFile(new File(mangledMCFile))
  }

  def readMangledNullCheck() = {
    val aglFolder = System.getProperty(S3Properties.ANGELIX_FOLDER)
    val mangledNCKFile = aglFolder + File.separator + S3Properties.MANGLED_NULL_CHECK_FILE
    NameMangling.readMangledNullCheckFromFile(new File(mangledNCKFile))
  }

  private def replaceConstants(content: String): String = {
    content.replace("(0 &&", "(false &&")
        .replace("&& 0)", "&& false)")
        .replace("|| 0)", "|| false)")
        .replace("(0 ||", "(false ||")
        .replace("&& 1)", "&& true)")
        .replace("(1 &&","(true &&")
        .replace("|| 1)", "|| true)")
        .replace("(1 ||", "(true ||")
  }

  def applyPatch(file2Patch: String, patchFile: String): Array[URL] = {
    val lines = mylib.Lib.readFile2Lines(new File(patchFile)).filter(p => p.trim.compareTo("") != 0)
    var index = 0
    var innerIndex = 0
    var currentFixLocation: Array[String] = null
    val backendInstrument = new BackEndInstrument(null)
    readMangledMethodCall()
    readMangledNullCheck()

    while (index<lines.length){
      val currentLine = lines(index)
      if(currentLine.matches("\\d+-\\d+-\\d+-\\d+")){
        logger.info("Replace at: "+currentLine)
        currentFixLocation = currentLine.split("-")
        innerIndex = 0
      }else{
        innerIndex += 1
        if(innerIndex == 1)
          logger.info("Being replaced: "+currentLine)
        else if(innerIndex == 2){
          var repContent = ""
          try{
            val boolValue = Integer.parseInt(currentLine)
            if(boolValue == 1)
              repContent = "true"
            else if(boolValue == 0)
              repContent = "false"
            else throw new RuntimeException("Did not parse either 1 -> true or 0 -> false")
          }catch {
            case e: Exception => {
              repContent = replaceConstants(currentLine)
              // Method call with mangled name like the pattern bellow. E.g., MC_12345
              // @See NameMangling
              val m = NameMangling.methodCallPattern.matcher(repContent)
              while (m.find()) {
                val s = m.group(0)
                val correspondingMC = NameMangling.mangledMethodCallKeyName.get(s)
                repContent = repContent.replace(s, correspondingMC)
              }

              val nck1 = NameMangling.nullCheckPatternEQ.matcher(repContent)
              while (nck1.find()) {
                val s = nck1.group(0)
                val varName = NameMangling.mangledNullCheckKeyName.get(s)
                val correspondingNCK = varName +" == "+"null"
                repContent = repContent.replace(s, correspondingNCK)
              }

              val nck2 = NameMangling.nullCheckPatternNEQ.matcher(repContent)
              while (nck2.find()) {
                val s = nck2.group(0)
                val varName = NameMangling.mangledNullCheckKeyName.get(s)
                val correspondingNCK = varName +" != "+ "null"
                repContent = repContent.replace(s, correspondingNCK)
              }

              /*val vp = NameMangling.mangledVarPattern.matcher(repContent)
              while (vp.find()) {
                val s = vp.group(0)
                val varName = NameMangling.mangledVarKeyName.get(s)
                repContent = repContent.replace(s, varName)
              }*/
            }
          }

          val repNode = ASTUtils.createNodeFromString(repContent)
          logger.info("Replacement: "+repNode)
          val cu = JavaParser.getCompilationUnit(file2Patch)
          val faultIdentifier = new FaultIdentifier(currentFixLocation)
          val toBeRepNode = ASTUtils.findNode(cu, faultIdentifier)
          backendInstrument.applyChange(file2Patch, toBeRepNode, repNode)
        }else{
          throw new RuntimeException("Should never happen because: line1 = line number, line2 = node being replace, line3 = rep node!")
        }
      }
      index += 1
    }
    backendInstrument.compile()
    backendInstrument.classPaths()
  }
}
