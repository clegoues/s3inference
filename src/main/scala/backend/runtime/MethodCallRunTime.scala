package backend.runtime

import java.util

import backend.NameMangling
import driver.Options
import frontend.Repairable
import localization.Identifier
import main.scala.utils.{MethodCallUtils, UsedMethodCalls}
import org.eclipse.jdt.core.dom._
import utils.{InstrumentUtils, JDTNameWrapper}

import scala.collection.mutable.ArrayBuffer
/**
  * Created by dxble on 11/9/16.
  */
class MethodCallRunTime (cu: CompilationUnit, fault: Identifier[Any]){

  import scala.collection.JavaConversions._
  lazy val availMethCurClass = methodsCurrentClCanHandle()

  lazy val availMethCurMethVars = methodsAvailInVisibleVars(fault.getLocalVisibleVars())
  lazy val availMethStaticFields = methodsAvailInVisibleVars(fault.getStaticFieldVars())
  lazy val availMethNonStaticFields = methodsAvailInVisibleVars(fault.getNonStaticFieldVars())
  lazy val availMethUsedVars = methodsAvailInVisibleVars(fault.getUsedVarsAtFault().foldLeft(new util.HashMap[JDTNameWrapper, util.ArrayList[ITypeBinding]]())(
    (res, v) => {
      val al = new util.ArrayList[ITypeBinding]()
      al.add(v._2)
      res.put(v._1, al)
      res
  }))

  lazy val usedMethAtFault = {
    val usedMeth = new UsedMethodCalls()
    fault.getJavaNode().accept(usedMeth)
    //usedMeth.usedMethodCalls.keySet.toArray
    usedMeth.usedMethodCalls
  }

  /**
    * @param envIDs
    * @param envVals
    *                Collect all methods available in the variables used at fault
    */
  def methInUsedVar(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding]) = {
    availMethUsedVars.foreach(f => {
      val varName = f._1
      val methods = f._2.filter(m => m.getParameterTypes.size <= Options.methodCallNumberParams)
      methods.foreach(m => {
        if(m.getParameterTypes.size == 0){
          val subsMI = cu.getAST.newMethodInvocation()
          subsMI.setExpression(ASTNode.copySubtree(subsMI.getAST, varName.name).asInstanceOf[Expression])
          subsMI.setName(ASTNode.copySubtree(subsMI.getAST, cu.getAST.newSimpleName(m.getName)).asInstanceOf[SimpleName])
          val mcallSubst = subsMI.toString
          val mangledName = NameMangling.mangleMethodCall(mcallSubst)
          if(!envIDs.contains(mangledName)) {
            envIDs.add(mangledName)
            envVals.put(mcallSubst, m.getReturnType)
            InstrumentUtils.createdMethodCalls.put(mcallSubst, subsMI)
          }
        }
      })
    })
  }

  private def methodsCurrentClCanHandle() = {
    def filterMethods(mDec: MethodDeclaration): Boolean = {
      val st = if(fault.surroundingMethodIsStatic()) Modifier.isStatic(mDec.getModifiers) else true
      val surroundingFault = fault.getSurroundingMethod().toString.compareTo(mDec.resolveBinding().toString) == 0
      st && !surroundingFault && !Modifier.isAbstract(mDec.getModifiers) && !mDec.resolveBinding().isConstructor && !mDec.resolveBinding().isDefaultConstructor
    }

    cu.types().get(0).asInstanceOf[TypeDeclaration].getMethods.filter(methodDec => {
      filterMethods(methodDec) &&
      Repairable.canHandleType(methodDec.getReturnType2.resolveBinding()) &&
        !methodDec.resolveBinding().getParameterTypes.exists(t => {
          !Repairable.canHandleType(t)
        }) &&
        !Options.methodCallsNotCollect.contains(methodDec.getName.getIdentifier)
    }).map(m => m.resolveBinding())
  }

  private def methodsAvailInVisibleVars(variables: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]) = {
    // This returns variable names with methods available in it
    def methCanbeUsed(m: IMethodBinding) = {
      !m.isDefaultConstructor && !m.isConstructor && Modifier.isPublic(m.getModifiers) && !Modifier.isAbstract(m.getModifiers) && Repairable.canHandleType(m.getReturnType) &&
        !m.getParameterTypes.exists(mt => {
          !Repairable.canHandleType(mt)
        })
    }
    val varMt = variables.map(v => {
      val objVars = v._2.filter(t => t.isClass)
      val availMethods = objVars.map(t => t.getDeclaredMethods.filter(m => methCanbeUsed(m))).flatten
      val superClassMeths = objVars.map(t => {
        val mt = new ArrayBuffer[IMethodBinding]()
        for(superClassLevel <- 1 to Options.methodCallSuperClassLevel){
          val superCl = t.getSuperclass
          if(superCl != null){
            superCl.getDeclaredMethods.foldLeft(mt)((res, m) =>{
              if(methCanbeUsed(m)){
                res.append(m)
              }
              res
            })
          }
        }
        mt
      }).flatten
      if(superClassMeths.size != 0)
        availMethods.appendAll(superClassMeths)

      (v._1, availMethods)
    })
    varMt
  }

  def methAtFaultUsedElseWhere(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding], surroundingClass: ASTNode) = {
    val usedMethodCalls = new UsedMethodCalls
    surroundingClass.accept(usedMethodCalls)
    usedMethAtFault.foreach(u => {
      val (binding, misAtFault) = u
      misAtFault.foreach(mi => {
        val key = MethodCallUtils.methodInvocationSig(mi, binding.getReturnType.toString)
        val mis = usedMethodCalls.usedMethodCallsWithSig.getOrElse(key, Set())
        mis.foreach(m => {
          //if(m.toString.compareTo(mi.toString) != 0) {
            val subMi = ASTNode.copySubtree(cu.getAST, m).asInstanceOf[MethodInvocation]
            val mcallSubst = subMi.toString
            val mangledName = NameMangling.mangleMethodCall(mcallSubst)
            if (!envIDs.contains(mangledName)) {
              envIDs.add(mangledName)
              envVals.put(mcallSubst, m.resolveMethodBinding().getReturnType)
              InstrumentUtils.createdMethodCalls.put(mcallSubst, subMi)
            }
          //}
        })
      })
    })
  }

  // Assumed the used method has at least 1 param
  def usedMethodCallsRep1Param(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding]) = {
    usedMethAtFault.foreach(f =>{
      val (m, methInvocs) = f
      methInvocs.foreach(mi => {
        // Example: a.getValue(x,y,z) => origExps ={x,y,z}, substs = [[sub_of_x], [sub_of_y], [sub_of_z]]
        val (substs, origExps) = mi.arguments().foldLeft((List[List[String]](), List[Expression]())) {
          (res, arg) => {
            val exps = res._2 :+ arg.asInstanceOf[Expression]
            if(arg.asInstanceOf[Expression].resolveConstantExpressionValue() == null){
              val t = arg.asInstanceOf[Expression].resolveTypeBinding()
              val sb = substitutableVars(fault.getLocalVisibleVars(), t)
              //if(Options.collectFields){
                val sb2 = substitutableVars(fault.getStaticFieldVars(), t)
                val sb3 = substitutableVars(fault.getNonStaticFieldVars(), t)
                val all = sb ::: sb2 ::: sb3
                (res._1 :+ all, exps)
              //}else {
              //  (res._1 :+ sb, exps)
              //}
            }else (res._1 :+ List(), exps)
          }
        }
        val allSubst = ArrayBuffer[List[Expression]]()
        for(index <- 0 to origExps.size-1){
          val substAtIndex = substs(index)
          if(substAtIndex.size > 0){
            val leftOrig = origExps.take(index)
            val rightOrig = origExps.takeRight(origExps.size-index-1)
            substAtIndex.foreach{
              s => {
                val aSub = (leftOrig :+ InstrumentUtils.createNameFromString(s, cu)) ::: rightOrig
                allSubst.append(aSub)
              }
            }
          }
        }
        def createMi() = {
          val subsMI = cu.getAST.newMethodInvocation()
          subsMI.setExpression(ASTNode.copySubtree(subsMI.getAST, mi.getExpression).asInstanceOf[Expression])
          subsMI.setName(ASTNode.copySubtree(subsMI.getAST, mi.getName).asInstanceOf[SimpleName])
          subsMI
        }
        allSubst.foreach(sub => {
          val subMi = createMi()
          sub.foreach(arg => {
            val argCopied = ASTNode.copySubtree(cu.getAST, arg).asInstanceOf[Expression]
            InstrumentUtils.addArgument2MethodCall(subMi, argCopied)
          })
          val mcallSubst = subMi.toString
          val mangledName = NameMangling.mangleMethodCall(mcallSubst)
          if(!envIDs.contains(mangledName)) {
            envIDs.add(mangledName)
            envVals.put(mcallSubst, m.getReturnType)
            InstrumentUtils.createdMethodCalls.put(mcallSubst, subMi)
          }
        })
      })
    })
  }

  def methodsInCurrentClass(envIDs: java.util.ArrayList[String],
                            envVals: java.util.LinkedHashMap[String, ITypeBinding]) = {
    def nameOfMethodBinding(m: IMethodBinding) = Map(m.getName -> (null, cu.getAST.newSimpleName(m.getName)))
    methodCallRunTimeCollector(envIDs, envVals, availMethCurClass, nameOfMethodBinding)
  }

  def encodingOnly(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding]) = {
    usedMethAtFault.foreach(f =>{
      val (m, methInvocs) = f
      methInvocs.foreach(mi => {
        val mcallSubst = mi.toString
        val mangledName = NameMangling.mangleMethodCall(mcallSubst)
        if(!envIDs.contains(mangledName) && Repairable.isPrimitiveType(mi.resolveMethodBinding().getReturnType)) {
          envIDs.add(mangledName)
          envVals.put(mcallSubst, m.getReturnType)
          InstrumentUtils.createdMethodCalls.put(mcallSubst, ASTNode.copySubtree(cu.getAST, mi).asInstanceOf[MethodInvocation])
        }
      })
    })
  }

  def usedMethods(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding]) = {
    val mbindings = usedMethAtFault.keySet.toArray
    val x = usedMethAtFault.map(m => {
      val k = m._2.map(mi => {
        val exp = mi.getExpression
        val name = mi.getName
        if (exp != null)
          (exp.toString + "." + name.toString, (exp, name))
        else (name.toString, (exp, name))
      }).toMap
      (m._1, k)
    })

    def nameOfMethodBinding(m: IMethodBinding) = x.getOrElse(m, null)
    methodCallRunTimeCollector(envIDs, envVals, mbindings, nameOfMethodBinding)
  }

  private def substitutableVars(toFind: util.HashMap[JDTNameWrapper, util.ArrayList[ITypeBinding]], t: ITypeBinding)
  : List[String] = {
    // Find all vars that have same type with this parameter type t
    toFind.foldLeft(List[String]())(
      (res, v) => {
        val name = v._1.name.getFullyQualifiedName
        if(v._2.exists(vt => compatibleTypes(vt, t)))
          name :: res
        else res
      })
  }

  def methodCallRunTimeCollector(envIDs: java.util.ArrayList[String],
                       envVals: java.util.LinkedHashMap[String, ITypeBinding], availMeth: Array[IMethodBinding], nameOfMBinding: IMethodBinding => Map[String, (Expression, SimpleName)]) = {
    def createSubst(possibleSubst: List[String], m : IMethodBinding, mcallSubst: String, exp: Expression, name: SimpleName): Unit = {
      val mangledName = NameMangling.mangleMethodCall(mcallSubst)
      if(!envIDs.contains(mangledName)){
        envIDs.add(mangledName)
        envVals.put(mcallSubst, m.getReturnType)
        val mi = cu.getAST.newMethodInvocation()
        possibleSubst.foreach(ps => InstrumentUtils.addArgument2MethodCall(mi, string2JDTNode(ps)))
        if(exp != null)
          mi.setExpression(ASTNode.copySubtree(mi.getAST, exp).asInstanceOf[Expression])
        mi.setName(ASTNode.copySubtree(mi.getAST, name).asInstanceOf[SimpleName])
        InstrumentUtils.createdMethodCalls.put(mcallSubst, mi)
      }
    }

    availMeth.foreach(m => {
      val substs = m.getParameterTypes.foldLeft(List[List[String]]()) {
        (res, t) => {
          val sb = substitutableVars(fault.getLocalVisibleVars(), t)
          val sb2 = substitutableVars(fault.getStaticFieldVars(), t)
          val sb3 = substitutableVars(fault.getNonStaticFieldVars(), t)
          val all = sb ::: sb2 ::: sb3
          if (all.size > 0)
            res :+ all
          else res
        }
      }

      // If we can find possible substitutions of all parameters
      val collect: List[List[String]] = List(List())
      if (substs.size == m.getParameterTypes.size) {
        val zss = substs.foldRight(collect)(crossP _)
        if (zss.size == 0) {
          nameOfMBinding(m).foreach(name => {
            val mcallSubst = name._1 + "()"
            createSubst(List[String](), m, mcallSubst, name._2._1, name._2._2)
          })
        }
        else
          zss.foreach(possibleSubst => {
            nameOfMBinding(m).foreach(name => {
              val mcallSubst = name._1 + "(" + possibleSubst.mkString(",") + ")"
              createSubst(possibleSubst, m, mcallSubst, name._2._1, name._2._2)
            })
          })
      }
    })
  }

  private def string2JDTNode(s: String): ASTNode = {
    if(!s.contains("\\.")){
      return cu.getAST.newSimpleName(s)
    }else {
      if(s.contains("this")) {
        val fa: FieldAccess = cu.getAST.newFieldAccess
        fa.setExpression(cu.getAST.newThisExpression)
        fa.setName(cu.getAST.newSimpleName(s.replace("this.", "")))
        return fa
      }else{
        cu.getAST.newName(s)
      }
    }
  }
  // Cross product
  def crossP(xs: List[String], zss: List[List[String]]): List[List[String]] = {
    for {
      x <- xs
      zs <- zss
    } yield {
      x :: zs
    }
  }

  private def compatibleTypes(t1: ITypeBinding, t2: ITypeBinding): Boolean ={
    if(t1 == null || t2 == null)
      return false

    // Char is only compatible with int
    if(Repairable.isCharType(t1.toString) && (!Repairable.isIntType(t2.toString) || !Repairable.isCharType(t2.toString)))
      return false
    if(Repairable.isCharType(t2.toString) && (!Repairable.isIntType(t1.toString) || !Repairable.isCharType(t1.toString)))
      return false

    if (t1.isSubTypeCompatible(t2) || t1.isCastCompatible(t2)
      || t1.isAssignmentCompatible(t2) || t1.toString.equals(t2.toString)) {
      return true
    }else
      return false
  }
}
