package backend.runtime

import java.util

import backend.NameMangling
import localization.Identifier
import org.eclipse.jdt.core.dom.{CompilationUnit, ITypeBinding, InfixExpression}
import utils.InstrumentUtils

/**
  * Created by dxble on 1/16/17.
  */
class ObjectRunTime (cu: CompilationUnit, fault: Identifier[Any]){

  // To encode null check as a boolean variable, and add to envID and envVal (value of the envID)
  def nullCheck(envIDs: util.ArrayList[String], envVals: util.LinkedHashMap[String, ITypeBinding], varName: String) = {
    val key = NameMangling.mangledNullCheckNameKey.get(varName)
    if(key == null){
      // Add mangledKey to envIDs
      val mangledKey = NameMangling.mangleNullCheck(varName, InfixExpression.Operator.NOT_EQUALS)
      envIDs.add(mangledKey)
    }else
      envIDs.add(key)

    // Now add varName != null to envVals
    val neqExp = cu.getAST.newInfixExpression()
    neqExp.setLeftOperand(neqExp.getAST.newName(varName))
    neqExp.setRightOperand(neqExp.getAST.newNullLiteral())
    neqExp.setOperator(InfixExpression.Operator.NOT_EQUALS)
    // Note: this binding is always null because it is not parsed from source code. See Repairable.type2TypeBoxEnum
    // TODO: see how to make this resolve
    val binding = neqExp.resolveTypeBinding()
    val expString = neqExp.toString
    envVals.put(expString, binding)

    // Memorizing created null check for reuse in InstrumentUtils.createChooseNode later
    InstrumentUtils.createdNullCheck.put(expString, neqExp)
  }
}
