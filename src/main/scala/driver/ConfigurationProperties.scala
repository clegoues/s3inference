package driver

/**
  * Created by dxble on 9/11/16.
  */

import org.apache.log4j.Logger
import java.io.FileInputStream
import java.io.InputStream
import java.util.Properties
case class ConfigurationException(message:String)  extends Exception(message)

object ConfigurationProperties {
  var properties: Properties = null
  protected var log: Logger = Logger.getLogger(this.getClass)

  def loadRequiredProps() = {

    // Absolutely required options
    Options.sourceFolder=getProperty("sourceFolder")
    Options.testFolder=getProperty("testFolder")
    Options.junitDeps=getProperty("junitDeps")
    Options.jpfDeps=getProperty("jpfDeps")

    Options.testClassDir=getProperty("testClassDir")
    Options.appClassDir=getProperty("appClassDir")
    Options.angelixRoot=getProperty("angelixRoot")
    val testDeps = if(Options.testClassDir == null) Options.testFolder else Options.testClassDir
    Options.dependencies=Options.angelixRoot+"/src/main/java;"+Options.appClassDir+";"+Options.angelixRoot+"/src/main/scala/;"+testDeps+";"+Options.junitDeps+";"+Options.jpfDeps
    val depTemp = getProperty("dependencies")
    if(depTemp != null){
      Options.dependencies += ";"+depTemp
    }

    val jvHome = getProperty("javaHome")
    if(jvHome != null)
      System.setProperty("java.home", jvHome)
    Options.localLibs=Options.jpfDeps
    val locLibTemp = getProperty("localLibs")
    if(locLibTemp != null)
      Options.localLibs += ";"+locLibTemp

    Options.faultFile=getProperty("faultFile")

    Options.synthesisLevels=getProperty("synthesisLevels")
    Options.synthesisJar=getProperty("synthesisJar")

    Options.assertFile=getProperty("assertFile")
    Options.failingTests=getProperty("failingTests")

    // End absolutely required options

    // Optional
    //val jv = ConfigurationProperties.getProperty("javaHome")
    //if(jv != null)
    //  System.setProperty("java.home", jv)

    val solver=getProperty("jpfSymbcSolver")
    if(solver != null)
      Options.jpfSymbcSolver=solver

    val faultLines=getProperty("faultLines")
    if(faultLines != null)
      Options.faultLines=faultLines
    val groupSize=getProperty("groupSize")
    if(groupSize != null)
      Options.groupSize=groupSize.trim.toInt

    val libs=getProperty("libs")
    if(libs != null)
      Options.libs=libs.split(";")

    val jpfSearchDepth=getProperty("jpfSearchDepth")
    if(jpfSearchDepth != null)
      Options.jpfSearchDepth=jpfSearchDepth.trim

    val debug=getProperty("debug")
    if(debug != null)
      Options.debug=debug.trim().toBoolean

    val defectClasses=getProperty("defectClasses")
    if(defectClasses != null)
      Options.defectClasses=defectClasses
    val incorrectDefectCl = Options.defectClasses.split(";").filterNot(p => {
      p.compareTo("assignments") == 0 || p.compareTo("if-conditions") == 0 || p.compareTo("loop-conditions") == 0 || p.compareTo("guards") == 0 || p.compareTo("methodCalls") == 0 || p.compareTo("returns") == 0 || p.compareTo("asserts") == 0 || p.compareTo("conditional-expressions") == 0
    })
    if(incorrectDefectCl.length > 0)
      throw new ConfigurationException("Defect classes do not allow: "+incorrectDefectCl.foldLeft(""){(res, i) => res+" "+i})
    if(Options.defectClasses.contains("guards") && Options.defectClasses.contains("assignments"))
      throw new ConfigurationException("Defect classes: guards and assignments are now incompatible with each other!")

    // *** Synthesis Options ****
    val synthesisSimplify=getProperty("synthesisSimplify")
    if(synthesisSimplify != null)
      Options.synthesisSimplification=synthesisSimplify.trim().toBoolean

    val spaceReduction=getProperty("spaceReduction")
    if(spaceReduction != null)
      Options.spaceReduction=spaceReduction.toBoolean

    val synthesisTimeout=getProperty("synthesisTimeout")
    if(synthesisTimeout != null)
      Options.synthesisTimeout=synthesisTimeout.trim().toInt

    val groupByScore=getProperty("groupByScore")
    if(groupByScore != null)
      Options.groupByScore=groupByScore.trim().toBoolean

    val skipConfigValidation=getProperty("skipConfigValidation")
    if(skipConfigValidation != null)
      Options.skipConfigValidation=skipConfigValidation.trim.toBoolean

    val jpfMinInt=getProperty("jpfMinInt")
    if(jpfMinInt != null)
      Options.jpfMinInt=jpfMinInt.trim.toInt

    val jpfMaxInt=getProperty("jpfMaxInt")
    if(jpfMaxInt != null)
      Options.jpfMaxInt=jpfMaxInt.trim.toInt

    val jpfMinDouble=getProperty("jpfMinDouble")
    if(jpfMinDouble != null)
      Options.jpfMinDouble=jpfMinDouble.trim.toInt

    val jpfMaxDouble=getProperty("jpfMaxDouble")
    if(jpfMaxDouble != null)
      Options.jpfMaxDouble=jpfMaxDouble.trim.toInt
  }

  def load(configFile: String) = {
    var propFile: InputStream = null
    try {
      properties = new Properties
      propFile = new FileInputStream(configFile)
      properties.load(propFile)
    }
    catch {
      case e: Exception => {
        e.printStackTrace
      }
    }
  }

  def clearConfig() = {
    properties.clear()
  }

  def hasProperty(key: String): Boolean = {
    if (properties.getProperty(key) == null) {
      return false
    }
    return true
  }

  def getProperty(key: String): String = {
    return properties.getProperty(key)
  }

  def getPropertyInt(key: String): Integer = {
    return Integer.valueOf(properties.getProperty(key))
  }

  def getPropertyBool(key: String): Boolean = {
    return properties.getProperty(key).toBoolean
  }

  def getPropertyDouble(key: String): Double = {
    return properties.getProperty(key).toDouble
  }

  def print() {
    log.info("----------------------------")
    log.info("---Configuration properties:---Execution values")
    import scala.collection.JavaConversions._
    for (key <- properties.stringPropertyNames) {
      log.info("p:" + key + "= " + properties.getProperty(key))
    }
    log.info("----------------------------")
  }
}
