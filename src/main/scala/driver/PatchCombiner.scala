package driver

import java.io.File

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import testrunner.TestCase

/**
  * Created by dxble on 10/10/16.
  */
abstract class PatchCombiner(angelixFolder: String, originalFailedTests: Array[TestCase[Any]], allTests: ArrayBuffer[TestCase[Any]]) {
  lazy val patchFolder = angelixFolder + File.separator + "tested"
  lazy val combinedPatchFolder = patchFolder + File.separator + "combined"
  lazy val patchTestInfor = new mutable.HashMap[String, Array[TestCase[Any]]]()

  /**
    * Combine patches that failed before
    *
    * @return list of combined patch stored in a folder
    */
  def combine(): ArrayBuffer[String] = {
    if(!new File(patchFolder).exists())
      return new ArrayBuffer[String]()

    val patches = mylib.Lib.search4Files(new File(patchFolder), "patch")
    import scala.collection.JavaConversions._
    val combinedPatch = patches.foldLeft(""){
      (res, p) =>{
        /**
          * Subclasses would reimplement satisfyCombineStrategy() accordingly
          */
        if(satisfyCombineStrategy(p.getCanonicalPath))
          res + mylib.Lib.readFile2String(p.getCanonicalPath) + "\n"
        else res
      }
    }
    val res = new ArrayBuffer[String]()
    if(combinedPatch != "") {
      new File(combinedPatchFolder).mkdirs()
      val combinedAllFile = combinedPatchFolder + File.separator + "combinedAll.patch"
      mylib.Lib.writeText2File(combinedPatch, new File(combinedAllFile))
      res.append(combinedAllFile)
    }
    res
  }

  def addPatchTest(patchName: String, failedTests: Array[TestCase[Any]]) = {patchTestInfor += patchName -> failedTests}
  protected def satisfyCombineStrategy(patchFile: String): Boolean
}

/**
  * This combiner combines all patches together
  * TODO: to implement other combiner that combines patches wisely, e.g.,
  * only combines patches related to each other, only takes patches that
  * are good enough, etc
  *
  * @param angelixFolder
  */
case class CombineAllPatch(angelixFolder: String, originalFailedTests: Array[TestCase[Any]], allTests: ArrayBuffer[TestCase[Any]]) extends PatchCombiner(angelixFolder, originalFailedTests, allTests){

  override def satisfyCombineStrategy(patchFile: String): Boolean = {
    // Only add if the number of failed tests by this patch is less than the number of originally failed tests
    val failedTests = patchTestInfor.getOrElse(patchFile, Array[TestCase[Any]]())
    if(failedTests.length > originalFailedTests.length || failedTests.length >= allTests.length/2)
      return false
    else if (failedTests.length < originalFailedTests.length)
      return true
    else{
      // Equal length and different set then return true
      originalFailedTests.exists(t => !failedTests.contains(t))
    }
  }
}
