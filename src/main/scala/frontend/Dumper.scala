package frontend

import java.io.{File, FileWriter}

import net.liftweb.json.JsonAST.{JField, JInt, JObject}
import org.apache.log4j.Logger
import net.liftweb.json.parse

class Dumper(workingDir: String, assertFilePath: String) {
  val logger = Logger.getLogger(this.getClass)

  def dumpExpectedValues(): Boolean = {
    val assertFile = new File(assertFilePath)
    if(!assertFile.exists()) {
      logger.info("Dumper will not dump expected output because assert file is not set.")
      return false
    }

    val pathDir = workingDir + File.separator + "dump"
    val dumpDir = new File(pathDir)
    if(dumpDir.exists()){
      dumpDir.delete()
    }
    dumpDir.mkdir()
    val assertContent = scala.io.Source.fromFile(assertFile).mkString
    val assertJson = parse(assertContent)
    logger.debug(assertJson)
    assertJson.asInstanceOf[JObject].values.foreach(f => {
      val testNumber = f._1
      val map = f._2.asInstanceOf[Map[String, List[JObject]]]
      val testDir = pathDir + File.separator + testNumber
      new File(testDir).mkdir()

      map.foreach {
        case (variableName, expectedValues) => {
          val varDir = testDir + File.separator + variableName
          new File(varDir).mkdir()
          expectedValues.zipWithIndex.foreach{ f => {
            //TODO: for now we only consider Integer (either int or char), later String needs to be consider if we can handle that
            val value = f._1.asInstanceOf[BigInt]
            val index = f._2
            val valueFile = varDir + File.separator + index
            mylib.Lib.writeText2File(value.toString(), new File(valueFile))
          }}
        }
      }
    })

    return true
  }
}
