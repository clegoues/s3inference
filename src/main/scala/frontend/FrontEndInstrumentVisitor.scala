package frontend

import java.io.{File, IOException}
import java.net.URL
import java.nio.file.{Files, Paths}

import driver.Options
import junithandler.compiler.JavaJDKCompiler
import org.apache.log4j.Logger
import org.eclipse.jdt.core.dom.rewrite.{ASTRewrite, ListRewrite}
import org.eclipse.jdt.core.dom._
import parser.javaparser.JavaParser
import utils.{ASTUtils, FileFolderUtils, InstrumentUtils}
import org.eclipse.jface.text.{BadLocationException, Document}
import org.eclipse.text.edits.{MalformedTreeException, TextEdit}

import scala.collection.mutable

/**
  * Created by dxble on 8/28/16.
  */
abstract class BaseFrontEndVisitor(fileName: String) extends ASTVisitor{
  val logger = Logger.getLogger(this.getClass)

  protected def insertTraceNode(typeBinding: ITypeBinding, node: ASTNode): Unit = {
    if(node == null || typeBinding == null)
      return
    if(!Repairable.canHandleType(typeBinding))
      return

    //TODO: consider later: typeBinding.isEnum || typeBinding.isNullType
    if(Repairable.isIntType(typeBinding.getName) /*|| typeBinding.getName.compareTo("Integer") == 0*/){
      val trace = InstrumentUtils.createTraceNode(node, "trace_int", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else if(Repairable.isBoolType(typeBinding.getName)){
      val trace = InstrumentUtils.createTraceNode(node, "trace_boolean", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else if(Repairable.isDoubleType(typeBinding.getName)){
      val trace = InstrumentUtils.createTraceNode(node, "trace_double", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else if(Repairable.isStringType(typeBinding.getName)){
      val trace = InstrumentUtils.createTraceNode(node, "trace_string", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else if(Repairable.isFloatType(typeBinding.getName)){
      val trace = InstrumentUtils.createTraceNode(node, "trace_float", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else if(Repairable.isCharType(typeBinding.getName)){
      val trace = InstrumentUtils.createTraceNode(node, "trace_char", true)
      val rewriter = getRewriterUptoDate()
      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.replaceNode(rewriter, node, trace))
    }
    else {
      throw new RuntimeException("Not yet handled type: "+typeBinding)
    }
  }

  //Utilities for rewriting ast
  protected def getRewriterUptoDate(): ASTRewrite = {
    if(!FrontEndInstrumentVisitor.modifiedRewriter.contains(fileName)){
      val cu = JavaParser.getCompilationUnit(fileName)
      val original: ASTRewrite = ASTRewrite.create(cu.getAST())

      // Insert the import tracewriter.TraceWriter to the rewriter
      val ip = InstrumentUtils.createImportTraceWriter(cu)
      val lrw = original.getListRewrite(cu, CompilationUnit.IMPORTS_PROPERTY)
      lrw.insertLast(ip, null)

      FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, original)
    }
    return FrontEndInstrumentVisitor.modifiedRewriter.getOrElse(fileName, null)
  }

  protected def applyEdits(rew: ASTRewrite) {
    try {
      val filePath = FileFolderUtils.fileNameAbsolutePath(fileName)
      val str: String = JavaParser.modifyingFiles.getOrElse(fileName, null)
      //System.out.println("Content: "+str)
      //println(JavaParser.modifyingFiles)
      val document: Document = new Document(str)
      val edits: TextEdit = rew.rewriteAST(document, null)
      try {
        edits.apply(document)
        val newSource: String = document.get
        //println("Writing to document: "+newSource)
        //Files.write(Paths.get(filePath),newSource.getBytes(StandardCharsets.UTF_8))
        System.out.println(newSource)
      }
      catch {
        case e: MalformedTreeException => {
          e.printStackTrace
        }
        case e: BadLocationException => {
          e.printStackTrace
        }
      }
    }
    catch {
      case e: IllegalArgumentException => {
        e.printStackTrace
      }
      case e1: IOException => {
        e1.printStackTrace
      }
    }
  }
}
class ExpressionFinder(fileName: String, insideParenthesized: Boolean) extends BaseFrontEndVisitor(fileName){
  var exp: Expression = null
  var parenthesizedExpression: Boolean = insideParenthesized

  def resolveExpTypeBinding(): ITypeBinding = {
    if(exp != null){
      if(exp.isInstanceOf[MethodInvocation])
        exp.asInstanceOf[MethodInvocation].resolveMethodBinding().getReturnType
      else exp.resolveTypeBinding()
    }else return null
      //throw new RuntimeException("Exp is null, cannot resolve type binding!")
  }

  override def visit(node: FieldAccess): Boolean ={
    exp = node
    false
  }

  override def visit(node: BooleanLiteral): Boolean = {
    exp = node
    false
  }

  override def visit(node: CharacterLiteral): Boolean = {
    exp = node
    false
  }

  override def visit(node: SimpleName): Boolean = {
    val binding = node.resolveBinding()
    if(binding.isInstanceOf[IVariableBinding]) {
      val typeBinding = node.resolveTypeBinding()
      if(typeBinding != null && Repairable.canHandleType(typeBinding))
        exp = node
    }
    false
  }

  override def visit(node: QualifiedName): Boolean = {
    val binding = node.resolveBinding()
    if(binding.isInstanceOf[IVariableBinding]) {
      val typeBinding = node.resolveTypeBinding()
      if(typeBinding != null && Repairable.canHandleType(typeBinding))
        exp = node
    }
    false
  }

  override def visit(node: NullLiteral): Boolean = {
    if(Options.repairNullness)
      exp = node
    false
  }

  override def visit(node: NumberLiteral): Boolean = {
    exp = node
    false
  }

  override def visit(node: PostfixExpression): Boolean = {
    exp = node
    false
  }

  override def visit(node: PrefixExpression): Boolean = {
    exp = node
    false
  }

  override def visit(node: StringLiteral): Boolean = {
    exp = node
    false
  }

  override def visit(node: ThisExpression): Boolean = {
    exp = node
    false
  }

  override def visit(node: MethodInvocation): Boolean = {
    if(Options.defectClasses.contains("methodCalls")) {
      try {
        // We only take methods whose return type we can handle, e.g., int, boolean, char, etc
        if (Repairable.canHandleType(node.resolveMethodBinding().getReturnType))
          exp = node
      }catch {
        case e: Exception =>{
          logger.info("debug")
        }
      }
    }
    false
  }

  override def visit(node: ParenthesizedExpression): Boolean = {
    val expression = node.getExpression
    val visitor = new ExpressionFinder(fileName, true)
    expression.accept(visitor)
    if(visitor.exp != null)
      exp = node
    false
  }

  override def visit(node: InstanceofExpression): Boolean = {
    //TODO: Not yet handled instanceofExp
    false
  }

  override def visit(node: InfixExpression): Boolean = {
    val left = node.getLeftOperand
    val right = node.getRightOperand
    val leftVisitor = new ExpressionFinder(fileName, parenthesizedExpression)
    left.accept(leftVisitor)
    val rightVisitor = new ExpressionFinder(fileName, parenthesizedExpression)
    right.accept(rightVisitor)
    if(leftVisitor.exp != null && rightVisitor.exp != null){
      exp = node
    }else if(leftVisitor.exp != null && !parenthesizedExpression){
      if(node.getOperator != InfixExpression.Operator.EQUALS && node.getOperator != InfixExpression.Operator.NOT_EQUALS)
        exp = leftVisitor.exp
    }else if(rightVisitor.exp != null && !parenthesizedExpression){
      if(node.getOperator != InfixExpression.Operator.EQUALS && node.getOperator != InfixExpression.Operator.NOT_EQUALS)
        exp = rightVisitor.exp
    }
    false
  }
}
class FrontEndInstrumentVisitor(fileName: String)  extends BaseFrontEndVisitor(fileName){

  /*override def preVisit(node: ASTNode) ={
    logger.info("Visiting: "+node)
  }*/

  private def insertTraceNodeStatementLevel(node: ASTNode): Unit = {
    val trace = InstrumentUtils.createTraceNode(node, "trace_writer", false)
    val rewriter = getRewriterUptoDate()
    FrontEndInstrumentVisitor.modifiedRewriter.put(fileName, ASTUtils.insertBeforeNode(rewriter, node, node.getAST.newExpressionStatement(trace)))
  }

  override def endVisit(node: ExpressionStatement): Unit = {
    //println("debug")
  }

  override def visit(node: Assignment): Boolean ={
    // For now we do not handle RHS as method call
    if(!Options.defectClasses.contains("methodCalls") && node.getRightHandSide.isInstanceOf[MethodInvocation])
      return false
    if(node.getLeftHandSide.isInstanceOf[ArrayAccess])
      return false

    if(Options.notInstrumentVars.contains(node.getLeftHandSide.toString))
      return false

    if(Options.defectClasses.contains("guards")){
      insertTraceNodeStatementLevel(node.getParent)
    }
    else if(Options.defectClasses.contains("assignments")) {
      val expFinder = new ExpressionFinder(fileName, false)
      node.getRightHandSide.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null)
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    true
  }

  // Instrument condition of IfStatement
  override def visit(node: IfStatement): Boolean = {
    val cond = node.getExpression

    if(Options.defectClasses.contains("if-conditions")) {
      val expFinder = new ExpressionFinder(fileName, false)
      cond.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null)
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    if(Options.defectClasses.contains("guards"))
      insertTraceNodeStatementLevel(node)
    true
  }

  // Instrument condition of Conditional Expression
  override def visit(node: ConditionalExpression): Boolean = {
    val cond = node.getExpression

    if(Options.defectClasses.contains("conditional-expressions")) {
      val expFinder = new ExpressionFinder(fileName, false)
      cond.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null)
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    // Note: we do not do guards like IfStatement above
    true
  }

  // Instrument condition of WhileStatement
  override def visit(node: WhileStatement): Boolean = {
    val cond = node.getExpression
    if(Options.defectClasses.contains("loop-conditions")) {
      val expFinder = new ExpressionFinder(fileName, false)
      cond.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null)
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    if(Options.defectClasses.contains("guards"))
      insertTraceNodeStatementLevel(node)
    true
  }

  // Instrument condition of ForStatement
  override def visit(node: ForStatement): Boolean = {
    val cond = node.getExpression
    if(Options.defectClasses.contains("loop-conditions")) {
      val expFinder = new ExpressionFinder(fileName, false)
      cond.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null)
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    if(Options.defectClasses.contains("guards"))
      insertTraceNodeStatementLevel(node)
    true
  }

  override def visit(node: MethodInvocation): Boolean = {
    if(Options.defectClasses.contains("guards")){
      if(node.getParent.isInstanceOf[ExpressionStatement] && !node.toString.contains("angelixChoose") &&
         !node.toString.contains("angelixReachable")){
        insertTraceNodeStatementLevel(node.getParent.asInstanceOf[ExpressionStatement])
      }
    }
    true
  }

  // Instrument expression in return statement
  override def visit(node: ReturnStatement): Boolean = {
    val cond = node.getExpression
    if(cond == null)
      return false

    if(Options.defectClasses.contains("returns")) {
      val expFinder = new ExpressionFinder(fileName, false)
      cond.accept(expFinder)
      if(expFinder.exp != null) {
        val typeBinding = expFinder.resolveExpTypeBinding()
        if (typeBinding == null || !Repairable.isPrimitiveType(typeBinding))
          return true
        insertTraceNode(typeBinding, expFinder.exp)
      }
    }
    if(Options.defectClasses.contains("guards"))
      insertTraceNodeStatementLevel(node)
    true
  }


  override def visit(fieldDeclaration: FieldDeclaration): Boolean ={
    Options.allowRepairField
  }

  override def visit(node: VariableDeclarationFragment): Boolean = {
    val init = node.getInitializer
    if(init != null){
      val typeBinding = init.resolveTypeBinding()
      if( typeBinding == null || init.isInstanceOf[ArrayCreation])
        return false

      if(Options.defectClasses.contains("assignments")) {
        val expFinder = new ExpressionFinder(fileName, false)
        init.accept(expFinder)
        if(expFinder.exp != null) {
          val typeBinding = expFinder.resolveExpTypeBinding()
          if (typeBinding == null)
            return true
          insertTraceNode(typeBinding, expFinder.exp)
        }
      }
    }
    true
  }

  override def visit(node: AssertStatement): Boolean = {
    val exp = node.getExpression
    if(exp == null)
      return false
    val expBinding = exp.resolveTypeBinding()
    if(expBinding == null)
      return false

    if(Options.defectClasses.contains("asserts")){
      val expFinder = new ExpressionFinder(fileName, false)
      exp.accept(expFinder)
      if(expFinder.exp != null) {
        //val typeBinding = expFinder.resolveExpTypeBinding()
        //if (typeBinding == null)
        //  return true
        insertTraceNode(expBinding, expFinder.exp)
      }
    }
    false
  }

}
// This object here is for the purpose of multiple-file repair
// Thus, the rewriter must be global to keep track of changes in many files
object FrontEndInstrumentVisitor{
  val logger = Logger.getLogger(this.getClass)
  val modifiedRewriter:  mutable.LinkedHashMap[String, ASTRewrite] = new mutable.LinkedHashMap[String, ASTRewrite]()
  val modifiedRewriterContent:  mutable.LinkedHashMap[String, String] = new mutable.LinkedHashMap[String, String]()

  private def getModifiedRewriterContent(): mutable.LinkedHashMap[String, String] = {
    if(modifiedRewriterContent.isEmpty) {
      if (FrontEndInstrumentVisitor.modifiedRewriter.size == 0)
        throw new RuntimeException("Attempting to get content of empty rewriters!");

      val iter = modifiedRewriter.iterator
      while (iter.hasNext) {
        val ent = iter.next()
        //ASTRewrite originalRewriter = mapOriginalRewriter.get(ent.getKey());//key is fileName
        val originalContent = JavaParser.modifyingFiles.getOrElse(ent._1, null)
        //System.out.println(JavaParser.modifyingFiles());
        val modifiedDoc = new org.eclipse.jface.text.Document(originalContent)
        val rewriter =  modifiedRewriter.getOrElse(ent._1, null)
        val edits = rewriter.rewriteAST(modifiedDoc, null)
        edits.apply(modifiedDoc)
        val modifiedContent = modifiedDoc.get()
        modifiedRewriterContent.put(ent._1, modifiedContent)
      }
    }
    return modifiedRewriterContent
  }

  def classPaths() = {
    val bytecodeOutput: String = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    val variantOutputFile: File = new File(bytecodeOutput)
    var bc: Array[URL] = null
    val originalURL: Array[URL] = utils.FileFolderUtils.getURLforInstrumented()
    bc = utils.FileFolderUtils.redefineURL(variantOutputFile, originalURL)
    bc
  }

  def compile(): Unit ={
    val bc = classPaths()
    val classPathString = bc.foldLeft(new java.util.ArrayList[String]()){
      (res, path) =>{
        res.add(path.toString)
        res
      }
    }

    val path2InstrumentedClassFiles = utils.FileFolderUtils.getInstrumentedDirWithPrefix()
    import scala.collection.JavaConversions._
    val fileSourceMap = getModifiedRewriterContent
    val compiler: JavaJDKCompiler = new JavaJDKCompiler(path2InstrumentedClassFiles,classPathString,fileSourceMap, null, null)
    val status=compiler.compile()
    if(status != JavaJDKCompiler.Status.COMPILED) {
      throw new RuntimeException("!!! Instrumentation does not compile...")
    }else{
      logger.info("Instrumentation Completed!")
    }
  }
}
