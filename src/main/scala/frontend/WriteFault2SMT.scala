package frontend

import java.io.{BufferedWriter, File, FileWriter, IOException}

import backend.NameMangling
import driver.S3Properties
import localization.FaultIdentifier
import org.eclipse.jdt.core.dom._
import utils.FileFolderUtils

import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 9/8/16.
  */
class WriteFault2SMT extends ASTVisitor{
  var smtString : String = ""
  var nullLitVisited : Boolean = false
  var noCombined : Boolean = false

  private def createSMT(wrappedByNOT: Boolean,op: String, left:String, right:String) ={
    val res = "("+op + " " + left + " " + right+")"
    if(wrappedByNOT)
      "(not "+res+")"
    else
      res
  }

  override def visit(node: InfixExpression): Boolean = {
    val leftVisitor = new WriteFault2SMT
    node.getLeftOperand.accept(leftVisitor)
    val rightVisitor = new WriteFault2SMT
    node.getRightOperand.accept(rightVisitor)

    var operator: String = node.getOperator.toString
    var wrappedByNOTOperator = false
    if(node.getOperator == InfixExpression.Operator.NOT_EQUALS) {
      operator = "="
      wrappedByNOTOperator = true
    }
    else if(node.getOperator == InfixExpression.Operator.EQUALS)
      operator="="
    else if(node.getOperator == InfixExpression.Operator.CONDITIONAL_AND)
      operator="and"
    else if(node.getOperator == InfixExpression.Operator.CONDITIONAL_OR)
      operator="or"

    // Support null check, this is only allowed when repairNullness option is enabled during instrumentation
    // See repairNullness in Options and FrontEndInstrumentation
    // in repairNullness everything is encoded as object != null
    if(leftVisitor.nullLitVisited){
      smtString = NameMangling.mangleNullCheck(rightVisitor.smtString, InfixExpression.Operator.NOT_EQUALS)//node.getOperator
    }else if(rightVisitor.nullLitVisited){
      smtString = NameMangling.mangleNullCheck(leftVisitor.smtString, InfixExpression.Operator.NOT_EQUALS)
    }else
      smtString = createSMT(wrappedByNOTOperator, operator,leftVisitor.smtString, rightVisitor.smtString) //"("+operator + " " + leftVisitor.smtString + " " + rightVisitor.smtString+")"

    import scala.collection.JavaConversions._
    node.extendedOperands().foreach(op => {
      val visitor = new WriteFault2SMT
      op.asInstanceOf[ASTNode].accept(visitor)
      smtString = createSMT(false,operator, smtString, visitor.smtString)//"("+operator+" "+smtString+" "+visitor.smtString+" )"
    })
    return false
  }

  override def visit(node: PrefixExpression): Boolean = {
    val visitor = new WriteFault2SMT
    node.getOperand.accept(visitor)
    if(node.getOperator.equals(PrefixExpression.Operator.NOT))
      if(visitor.noCombined)
        smtString = "(not "+ visitor.smtString + ") "
      else
        smtString = "(not ("+ visitor.smtString + ")) "
    else smtString = node.toString

    return false
  }

  override def visit(node: MethodInvocation): Boolean = {
    val nameNoWhiteSpace = node.toString.trim.replaceAll("""(?m)\s+$""", "")
    val mangled = NameMangling.mangleMethodCall(nameNoWhiteSpace)
    smtString = mangled
    false
  }

  override def visit(node: CharacterLiteral): Boolean = {
    smtString = node.charValue().toInt.toString
    false
  }

  override def visit(node: ParenthesizedExpression): Boolean = {
    true
  }

  // This includes nodes such as SimpleName, NullLiteral, etc...
  override def preVisit2(node: ASTNode): Boolean = {
    if (node.isInstanceOf[NullLiteral]){
      nullLitVisited = true
      noCombined = true
      return false
    }

    //if(node.isInstanceOf[SimpleName] || node.isInstanceOf[QualifiedName])
    //  return true

    if(!node.isInstanceOf[InfixExpression] && !node.isInstanceOf[PrefixExpression]
       && !node.isInstanceOf[ParenthesizedExpression] && !node.isInstanceOf[MethodInvocation] && !node.isInstanceOf[CharacterLiteral]) {
      smtString = node.toString
      noCombined = true
      return false
    }
    //Otherwise, continue to visit the current node
    return true
  }

}

object WriteFault2SMT {

  def convertJDTNode2SMT(node: ASTNode): String = {
    val visitor = new WriteFault2SMT
    node.accept(visitor)
    return visitor.smtString
  }

  def writeOriginalFault2Extracted(faultGroups: ArrayBuffer[ArrayBuffer[FaultIdentifier]]): Unit = {
    val extractedFolder = System.getProperty(S3Properties.ANGELIX_FOLDER) + File.separator + "extracted"
    val existingExtFolder = new File(extractedFolder)
    mylib.Lib.removeDirectory(existingExtFolder)
    existingExtFolder.mkdirs()

    faultGroups.foreach(group => {
      group.foreach(fault => {
        val id = fault.getBeginLine()+"-"+fault.getBeginColumn()+"-"+fault.getEndLine()+"-"+fault.getEndColumn()
        val fileName = extractedFolder+File.separator+id+".smt2"
        // Statement level fix is to try to change the condition a always true condition: (not (= 1 0))
        // to something else. This is intended for fix involving statement deletion, or adding guards to
        // a statement.
        val content = if(fault.getJavaNode().isInstanceOf[Statement]) "(not (= 1 0))"
                      else convertJDTNode2SMT(fault.getJavaNode())
        FileFolderUtils.write2File(fileName, "(assert "+content+")")
      })
    })
  }
}
