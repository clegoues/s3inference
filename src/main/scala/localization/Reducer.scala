package localization

import testrunner.{NegativeTest, TestCase}

import scala.collection.mutable.ArrayBuffer
import driver.Options
import org.apache.log4j.Logger

import scala.collection.mutable
/**
  * Created by dxble on 8/29/16.
  */
object Reducer {
  val logger = Logger.getLogger(this.getClass)

  type SourceNameFaultLocSet = mutable.HashMap[String, Set[FaultIdentifier]]

  // Now return either whole test suite or negative tests only
  def reduce(repairTests: ArrayBuffer[TestCase[Any]], posTraces: ArrayBuffer[Trace], negTraces: ArrayBuffer[Trace], expressions: ArrayBuffer[FaultIdentifier]): (ArrayBuffer[TestCase[Any]],ArrayBuffer[TestCase[Any]]) ={
    if(Options.inferNegativeTestsOnly)
      (repairTests.filter(t => t.isInstanceOf[NegativeTest]), new ArrayBuffer[TestCase[Any]]())
    else {
      if(Options.inferAllTests) {
        repairTests.partition(_.isInstanceOf[NegativeTest])
      }else if(Options.inferSpecificTests != null){
        (repairTests.filter(p => Options.inferSpecificTests.contains(p.getId().toString)), new ArrayBuffer[TestCase[Any]])
      }
      else{
        // We need to select best tests here, according to the number of initial tests used for inference.
        // Default, #initial tests is 2.
        val numberFailing = Math.sqrt(Options.inferInitialTests).toInt
        // This code was originally written for multiple files: as following the original angelix implementation
        val sourceName = ""
        val sourceDirs = List[String](sourceName)

        val relevant = expressions.toSet

        val data = new mutable.HashMap[TestCase[Any], SourceNameFaultLocSet]
        posTraces.foreach(pt => {
          val sf = new SourceNameFaultLocSet
          sf += sourceName -> (pt.traceList.toSet.intersect(relevant))
          data += pt.test -> sf
        })

        negTraces.foreach(nt => {
          val sf = new SourceNameFaultLocSet
          sf += sourceName -> (nt.traceList.toSet.intersect(relevant))
          data += nt.test -> sf
        })

        val currentCoverage = new mutable.HashMap[String, Set[FaultIdentifier]]()
        sourceDirs.foreach(s => currentCoverage += s -> (Set()))

        def selectBestTests(candidates: ArrayBuffer[TestCase[Any]], maxNumber: Int): ArrayBuffer[TestCase[Any]] = {
          val selected = new ArrayBuffer[TestCase[Any]]()
          var break = false
          for(i <- 0 to (maxNumber-1) if !break) {
            if (candidates.size == 0) {
              break = true
            } else {
              var bestIncrement = new mutable.HashMap[String, Int]()
              sourceDirs.foreach(s => bestIncrement += s -> 0)

              var bestIncrementTotal = 0
              var bestTest = candidates(0)

              var bestCoverage = 0
              var bestCoverageTest = bestTest

              candidates.foreach(test => {
                val currentIncrement = new mutable.HashMap[String, Int]()
                var currentIncrementTotal = 0
                var coverage = 0
                sourceDirs.foreach(source => {
                  val temp = data.getOrElse(test, new SourceNameFaultLocSet).getOrElse(source, Set())
                  currentIncrement += source -> (temp.diff(currentCoverage.getOrElse(source, Set())).size)
                  currentIncrementTotal += currentIncrement.getOrElse(source, 0)
                  coverage += temp.size
                })
                if (currentIncrementTotal > bestIncrementTotal) {
                  bestIncrement = currentIncrement
                  bestIncrementTotal = currentIncrementTotal
                  bestTest = test
                }
                if (coverage > bestCoverage) {
                  bestCoverage = coverage
                  bestCoverageTest = test
                }
              })

              if (bestIncrementTotal > 0) {
                selected.append(bestTest)
                candidates -= bestTest
                sourceDirs.foreach(source => {
                  currentCoverage.getOrElseUpdate(source, currentCoverage.getOrElse(source, Set()).union(data.getOrElse(bestTest, new SourceNameFaultLocSet).getOrElse(source, Set())))
                })
              } else if (bestCoverage > 0) {
                selected.append(bestCoverageTest)
                candidates -= bestCoverageTest
                sourceDirs.foreach(source => {
                  currentCoverage.update(source, data.getOrElse(bestCoverageTest, new SourceNameFaultLocSet).getOrElse(source, Set()))
                })
              }
              else
                break = true
            }
          }
          return selected
        }
        val selectedFailing = selectBestTests(negTraces.map(f => f.test), numberFailing)
        val selectedPassing = selectBestTests(posTraces.map(f => f.test), Options.inferInitialTests - numberFailing)
        val totalSelected = selectedFailing.size + selectedPassing.size
        logger.info(s"Selected $totalSelected tests")
        logger.info("Selected failing tests: "+selectedFailing)
        logger.info("Selected passing tests: "+selectedPassing)
        return (selectedFailing, selectedPassing)
      }
    }
  }
}
