package localization
import java.io.File

import driver.Options
import org.apache.log4j.Logger
import org.eclipse.jdt.core.dom._
import parser.javaparser.JavaParser
import testrunner.TestCase
import utils.{ASTUtils, JDTNameWrapper}

import scala.collection.mutable.{ArrayBuffer, HashMap}

/**
  * Created by dxble on 8/29/16.
  */
abstract class Identifier[+T] {
  // representation of a single statement, a statement can be determined as a java node,
  // can be determined as a line number, etc
  private var names: java.util.Set[String] = null
  private var scope: HashMap[String, ArrayBuffer[ITypeBinding]] = null
  protected var javaNode: ASTNode = null
  private var fixSpace: ArrayBuffer[Identifier[Any]] = null
  private var fixProb = 0.5
  private var triedTransform: Boolean = false
  protected var exps: Seq[Identifier[Any]] = null
  private var hasExpsInJavaNode: Boolean = false
  private var triedCollectExps = false
  private var possibleInvokers: ArrayBuffer[Expression] = null // for replacing invoker in method call
  private var possibleMethodCallRep: ArrayBuffer[SimpleName] = null // for replacing the method call name
  private var methodReturnType : ITypeBinding = null
  private var surroundingMethod: IMethodBinding = null
  private var methodIsStatic: Boolean = false
  private var triedCollectInvokers = false
  private var localVisibleVars : java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]] = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  private var nonStaticFieldVars : java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]] = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  private var staticFieldVars : java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]] = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  private var usedVarsAtFault: java.util.HashMap[JDTNameWrapper, ITypeBinding] = new java.util.HashMap[JDTNameWrapper, ITypeBinding]

  lazy val beginLine = getBeginLine().toInt
  lazy val endLine = getEndLine().toInt

  def setMethodIsStatic(st: Boolean) = methodIsStatic = st
  def surroundingMethodIsStatic(): Boolean = methodIsStatic

  def sameLocation(node: ASTNode): Boolean = {
    val id = ASTUtils.getFaultIdentifier(node)
    if(this.getBeginLine().trim().toInt != id.getBeginLine().trim().toInt || this.getBeginColumn().trim().toInt != id.getBeginColumn().trim().toInt ||
      this.getEndLine().trim().toInt != id.getEndLine().trim().toInt || this.getEndColumn().trim().toInt != id.getEndColumn().trim().toInt){
      return false
    }
    return true
  }

  def sameLocation(thatLoc: Array[String]): Boolean = {
    if(this.getBeginLine().trim().toInt != thatLoc(0).trim().toInt || this.getBeginColumn().trim().toInt != thatLoc(1).trim().toInt ||
      this.getEndLine().trim().toInt != thatLoc(2).trim().toInt  || this.getEndColumn().trim().toInt != thatLoc(3).trim().toInt){
      return false
    }
    return true
  }

  def getBeginLine(): String
  def getBeginColumn(): String
  def getEndLine(): String
  def getEndColumn(): String

  protected def getJavaNodeShortString(): String ={
    if(getJavaNode()!=null){
      if(getJavaNode().isInstanceOf[IfStatement]){
        var ifStr= ""
        ifStr = ifStr + "if("+getJavaNode().asInstanceOf[IfStatement].getExpression.toString+"){...}"
        return ifStr
      }

      if(getJavaNode().isInstanceOf[ForStatement]){
        var forStr= ""
        forStr = forStr + "for("+getJavaNode().asInstanceOf[ForStatement].getExpression.toString+"){...}"
        return forStr
      }

      if(getJavaNode().isInstanceOf[WhileStatement]){
        var whileStr= ""
        whileStr = whileStr + "while("+getJavaNode().asInstanceOf[WhileStatement].getExpression.toString+"){...}"
        return whileStr
      }

      return getJavaNode().toString
    }else{
      return null
    }
  }

  def setLocalVisibleVars(vars: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]) = localVisibleVars = vars
  def getLocalVisibleVars() = localVisibleVars
  def setNonStaticFieldVars(vars: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]) = nonStaticFieldVars = vars
  def setStaticFieldVars(vars: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]) = staticFieldVars = vars
  def getNonStaticFieldVars(): java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]] = nonStaticFieldVars
  def getStaticFieldVars() = staticFieldVars

  def setUsedVarsAtFault(varsUsedAtCurrent: java.util.HashMap[JDTNameWrapper, ITypeBinding]) = usedVarsAtFault = varsUsedAtCurrent
  def getUsedVarsAtFault() = usedVarsAtFault

  def setMethodReturnType(mtype: ITypeBinding) = this.methodReturnType = mtype
  def getMethodReturnType()= this.surroundingMethod.getReturnType

  def setSurroundingMethod(m: IMethodBinding) = this.surroundingMethod = m
  def getSurroundingMethod(): IMethodBinding = this.surroundingMethod

  def getFixSpace() = fixSpace
  def setFixSpace(fix: ArrayBuffer[Identifier[Any]]) = fixSpace = fix

  def getFixProb() = fixProb
  def setFixProb(prob: Double)= fixProb = prob

  def getMethodName(): String = {
    //throw new RuntimeException("Not supported")
    return "test/testgcd/gcd"
  }

  def isReturnStatement(): Boolean = {
    if (javaNode != null) {
      return javaNode.isInstanceOf[ReturnStatement]
    }else
      return false
  }

  // To be implemented by subclasses
  protected def setExps(expsInsideJavaNode: Seq[ASTNode]): Unit

  //def getExps(): Seq[Identifier[Any]] = exps

  def getScope(): HashMap[String, ArrayBuffer[ITypeBinding]] = {
    if(this.scope == null)
      this.setScope(ASTUtils.getScope(this.getJavaNode()))
    this.scope
  }
  def setScope(scp: HashMap[String, ArrayBuffer[ITypeBinding]]) = scope = scp
  def getJavaNode(): ASTNode = javaNode
  def setJavaNode(jvNode: ASTNode) = javaNode = jvNode

  def transformToJavaNode(): Boolean = {
    if(triedTransform && getJavaNode() == null)
      return false
    triedTransform = true
    if(getJavaNode()==null) {
      //sys.error("HAVE NOT SET THE Java Node for node: " + curNode)
      val desCUnit = JavaParser.globalASTs.getOrElse(getFileName(), null)
      //println("Finding: "+ getFileName())
      if(desCUnit == null) {
        println(getFileName() + "CU "+ desCUnit)
        println("This may mean we are getting AST node from TestCase!")
      }
      val jvNode= ASTUtils.findNode(desCUnit, this)
      if (jvNode == null)
        return false
      else {
        setJavaNode(jvNode)
        //println("Setting java node for: " + this + " as " + jvNode)
        return true
      }
    }else{
      return true
    }
  }

  def getLine(): Int
  def compareWith[B >:T](b:B): Boolean = true
  def getFileName(): String
  def getNames(): java.util.Set[String] = names
  def setNames(nameSet:  java.util.Set[String]): Unit = names = nameSet

  /*def isInScope[B >:T](toCheckNode: Identifier[B]): Boolean = {
    import scala.collection.JavaConversions._
    //this.hasExps()//collect exp for current node and collect field, method vars for current node
    for(checkName <- toCheckNode.getScope()){
      var inScope = true
      if(this.scope != null)
        inScope=this.scope.contains(checkName)
      if(this.fieldVars != null)
        inScope= inScope || this.fieldVars.contains(checkName)
      if(this.currentMethodVars != null)
        inScope= inScope || this.currentMethodVars.contains(checkName)
      if(!inScope)
        return false
    }
    return true
  }*/

  def isInScope[B >:T](toCheckNode: Name): Boolean = {
    import scala.collection.JavaConversions._

    var inScope = false
    if(this.scope != null)
      inScope=this.scope.contains(toCheckNode.getFullyQualifiedName)
    if(this.staticFieldVars != null)
      inScope= inScope || this.staticFieldVars.contains(new JDTNameWrapper(toCheckNode))
    if(this.nonStaticFieldVars != null)
      inScope= inScope || this.nonStaticFieldVars.contains(new JDTNameWrapper(toCheckNode))
    if(this.localVisibleVars != null)
      inScope= inScope || this.localVisibleVars.contains(new JDTNameWrapper(toCheckNode))
    if(!inScope)
      return false

    return true
  }

}
case class FaultIdentifier(iden: Array[String]) extends Identifier[Int] {

  override def toString(): String = iden.foldLeft("") {
    (res, i) => {
      res + i + " "
    }
  } + "JavaNode: " + getJavaNode()

  override def equals(that: Any): Boolean = {
    if (!that.isInstanceOf[FaultIdentifier])
      return false
    val castThat = that.asInstanceOf[FaultIdentifier]
    for (i <- 0 to iden.size - 1) {
      if (iden(i).compareTo(castThat.iden(i)) != 0)
        return false
    }
    return true
  }

  override def hashCode(): Int = {
    iden.foldLeft(0) { (res, i) => {
      res + i.hashCode()
    }
    }
  }

  override def getLine(): Int = Integer.valueOf(iden(0))

  override def getBeginLine(): String = iden(0)

  override def getBeginColumn(): String = iden(1)

  override def getEndLine(): String = iden(2)

  override def getEndColumn(): String = iden(3)

  def getFaultLocString(): String = iden(0) + "_" + iden(1) + "_" + iden(2) + "_" + iden(3)

  override def setExps(expsInsideJavaNode: Seq[ASTNode]): Unit = {}

  // TODO: now we only consider one fault file to fix at a time
  override def getFileName(): String = {
    Options.faultFile
  }
}

case class Trace(traceList: ArrayBuffer[FaultIdentifier], test: TestCase[Any]){
}
object Trace {
  val logger = Logger.getLogger(this.getClass)
  val testsHavingTrace = new ArrayBuffer[TestCase[Any]]
  private var traceFolderPath: String = null

  def getTraceFolderPath(): String = {
    if(traceFolderPath != null)
      return traceFolderPath

    val angelixFolder = System.getProperty("ANGELIX_FOLDER")
    traceFolderPath = angelixFolder + File.separator + "trace"
    return traceFolderPath
  }

  def parseTrace(test: TestCase[Any]): ArrayBuffer[FaultIdentifier] = {
    val traceFile = new File(getTraceFolderPath() + File.separator + test.getId())
    if(!traceFile.exists()) {
      //throw new RuntimeException("Trace file not found for test: "+test)
      logger.info("Trace file not found for test: "+test)
      return new ArrayBuffer[FaultIdentifier]()
    }

    testsHavingTrace.append(test)
    val lines = scala.io.Source.fromFile(traceFile).getLines()
    lines.foldLeft(ArrayBuffer[FaultIdentifier]()){
      (res, line) => {
        res.append(FaultIdentifier(line.split(' ')))
        res
      }
    }
  }
}
