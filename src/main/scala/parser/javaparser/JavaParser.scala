package parser.javaparser

/**
  * Created by dxble on 8/27/16.
  */
import java.io.{File}

import driver.Options
import utils.{FileFolderUtils}
import parser.AbstractParser
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.dom._

object JavaParser extends AbstractParser[CompilationUnit]{
  def parseOneFile(filePath: String, sourceFolder: String, unitName: String): (CompilationUnit, String) = {
    val content = readFileToString(filePath, null, false, sourceFolder)
    return (parse(content, filePath, sourceFolder, unitName), content)
  }

  def parse[B >:CompilationUnit](str: String, filePath: String, sourceFolder: String, unitName: String): CompilationUnit = {
    val parser: ASTParser = ASTParser.newParser(AST.JLS8)
    //parser.setEnvironment(RepairOptions.libs,Array(file.getParent, file.getParentFile.getParent), null, true)
    parser.setEnvironment(Options.getDependenciesList().toArray(Array[String]()),Array(new File(sourceFolder).getParent), null, true)
    parser.setUnitName(unitName)
    //parser.setEnvironment(Array(str), Array(str),Array("UTF-8"), true)
    parser.setSource(str.toCharArray)
    parser.setKind(ASTParser.K_COMPILATION_UNIT)
    parser.setResolveBindings(true)
    parser.setBindingsRecovery(true)
    parser.setStatementsRecovery(true)

    val options = JavaCore.getOptions()
    JavaCore.setComplianceOptions(JavaCore.VERSION_1_5, options);
    parser.setCompilerOptions(options)

    try {
      val root = parser.createAST(null)
      val cu: CompilationUnit = root.asInstanceOf[CompilationUnit]
      cu.recordModifications
      assert(cu != null)
      return cu
    }catch {
      case e: Throwable => {
        println(filePath)
        e.printStackTrace()
        sys.error("CU is null when parsing!!!"+filePath)
      }
    }
  }

  def getCompilationUnit(fileName: String): CompilationUnit ={
    globalASTs.getOrElse(fileName, null)
  }

  def getPackageName[B >:CompilationUnit](cu: CompilationUnit, filePath: String): String = {
    val pkg=cu.getPackage
    val file = new File(filePath)
    if(pkg!=null) {
      //val pkg_name=pkg.toString.stripLineEnd.split(" ")(1).split(";")(0)
      val pkg_name=pkg.getName.getFullyQualifiedName
      return pkg_name + "." + file.getName.split("\\.")(0)
    }else{
      return file.getName.split("\\.")(0) // this is to strip the .java at the end
    }
  }

  def batchParsingInDir(dirPath: String): Unit ={
    val files: java.util.List[File] = mylib.Lib.walk(dirPath,".java",new java.util.ArrayList[File])
    batchParseFiles(files)
  }

  override def batchParse(sourceFilePaths: Seq[String], faultFiles: Array[String]): Unit={

    val astParser = ASTParser.newParser(AST.JLS4)

    // set up libraries (.jar, .class or .java)
    // Array(new File(Options.sourceFolder).getParent)
    astParser.setEnvironment(Options.getDependenciesList().toArray(Array[String]()),Array(new File(Options.sourceFolder).toString), null, true)

    astParser.setResolveBindings(true)

    // with Bingding Recovery on, the compiler can detect
    // binding among the set of compilation units
    astParser.setBindingsRecovery(true)

    // set default options, especially for Java 1.5
    val options = JavaCore.getOptions()
    JavaCore.setComplianceOptions(JavaCore.VERSION_1_5, options)
    astParser.setCompilerOptions(options)


    val requestor = new FileASTRequestor() {
      override def acceptAST(sourceFilePath: String, ast: CompilationUnit) {
        val fileName=convertFilePath2FilePackageName(sourceFilePath)
        // if file being read is a fault file, keep its content
        // println("Processed FileName"+fileName)
        if(faultFiles.contains(fileName)) {
          readFileToString(sourceFilePath, faultFiles)
        }
        globalASTs.put(getPackageName(ast,sourceFilePath), ast)
      }

      override def acceptBinding(bindingKey: String, binding: IBinding) {
        // do nothing
        // System.out.println("Accept Binding:... " + bindingKey);
        // System.out.println(binding);
      }
    }

    astParser.createASTs(sourceFilePaths.toArray,
      null, 			/*  use default encoding */
      Array[String](), /* no binding key */
      requestor,
      null			/* no IProgressMonitor */
    )
  }

}
