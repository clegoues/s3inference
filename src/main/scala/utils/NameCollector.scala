package utils

/**
  * Created by dxble on 8/30/16.
  */
import org.eclipse.jdt.core.dom._
import java.util.Set
//remove if not needed

class NameCollector(private var nameSet: Set[String]) extends ASTVisitor {

  override def visit(node: SimpleName): Boolean = {
    val name = node.getIdentifier
    nameSet.add(name)
    super.visit(node)
  }
}
