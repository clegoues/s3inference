package main.scala.utils

import driver.Options
import frontend.Repairable
import org.eclipse.jdt.core.dom.{ASTVisitor, IMethodBinding, MethodInvocation}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 11/12/16.
  */
// TODO: this is currently applicable to be called at fault loc. However, to use this to collect
// method calls used in whole file, need to consider some other criteria
// @See VisibleVarsCollector where we collect used vars

class UsedMethodCalls extends ASTVisitor{
  val usedMethodCalls = new mutable.HashMap[IMethodBinding, mutable.HashSet[MethodInvocation]]()

  // Key is not signature, instead is expression.methodName_returnType
  val usedMethodCallsWithSig = new mutable.HashMap[String, mutable.HashSet[MethodInvocation]]()

  override def visit(node: MethodInvocation): Boolean = {
    val m = node.resolveMethodBinding()
    val retType = m.getReturnType.toString
    if(Repairable.canHandleType(m.getReturnType)){
      if(Options.noCareParamTypes || !m.getParameterTypes.exists(mt => {
        !Repairable.canHandleType(mt)})) {
        val v = usedMethodCalls.get(m).getOrElse(new mutable.HashSet[MethodInvocation]())
        v.add(node)
        usedMethodCalls.update(m, v)

        val key = MethodCallUtils.methodInvocationSig(node, retType)
        val v2 = usedMethodCallsWithSig.get(key).getOrElse(new mutable.HashSet[MethodInvocation]())
        v2.add(node)
        usedMethodCallsWithSig.update(key, v2)
      }
    }
    true
  }
}
object MethodCallUtils{
  def methodInvocationSig(node: MethodInvocation, retType: String): String = {
    var key = ""
    val exp = node.getExpression
    if(exp != null){
      key = exp.toString+"."+node.getName+"_"+retType
    }else key = node.getName.getIdentifier+"_"+retType
    return key
  }
}