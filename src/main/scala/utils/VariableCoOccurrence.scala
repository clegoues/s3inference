package utils

import org.eclipse.jdt.core.dom._
import org.jgrapht.graph.{DefaultWeightedEdge, SimpleGraph}

import scala.collection.mutable

/**
  * Created by dxble on 1/19/17.
  */
// faultFile in the form of package name, like abc.xyz.ClassNameHere
class VariableCoOccurrence(faultFile: String) extends ASTVisitor{
  val graph = new SimpleGraph[String, DefaultWeightedEdge](classOf[DefaultWeightedEdge])
  val coOccurVars: mutable.Stack[String] = new mutable.Stack()
  var toProcess = false
  // TODO: consider assignment, etc

  private def constructGraph(): Unit = {
    // Pop the coOccurVars to build graph
    while (!coOccurVars.isEmpty){
      val v = coOccurVars.pop()
      if(!graph.containsVertex(v)){
        graph.addVertex(v)
      }
      val iter = coOccurVars.iterator
      while(iter.hasNext){
        val anotherV = iter.next()
        if(!graph.containsVertex(anotherV)){
          graph.addVertex(anotherV)
        }
        if(!graph.containsEdge(anotherV, v)){
          val edge = graph.addEdge(anotherV, v)
          graph.setEdgeWeight(edge, 1.0)
        }else{
          val edge = graph.getEdge(anotherV, v)
          val w = graph.getEdgeWeight(edge)
          graph.setEdgeWeight(edge, w + 1.0)
        }
      }
    }
    assert(coOccurVars.size == 0)
  }

  override def visit(infixExpression: InfixExpression): Boolean = {
    toProcess = true
    true
  }

  override def endVisit(infixExpression: InfixExpression): Unit = {
    constructGraph()
    toProcess = false
  }

  override def visit(simpleName: SimpleName): Boolean = {
    // For now we do not care about method call's name
    if(toProcess && simpleName.resolveBinding().isInstanceOf[IVariableBinding]) {
      // Look for local vars first
      val wrapper = new JDTNameWrapper(simpleName)
      if (localVars.containsKey(wrapper)) {
        coOccurVars.push(simpleName.getIdentifier)
      } else if (staticFieldNames.containsKey(wrapper)) {
        coOccurVars.push(faultFile + "." + simpleName.getIdentifier)
      } else if (nonStaticFieldNames.containsKey(wrapper)) {
        coOccurVars.push("this." + simpleName.getIdentifier)
      }
    }
    false
  }

  override def visit(qualifiedName: QualifiedName): Boolean = {
    // For now we do not care about method call's name
    if(toProcess && qualifiedName.resolveBinding().isInstanceOf[IVariableBinding]) {
      val wrapper = new JDTNameWrapper(qualifiedName)
      // Look for nonStaticFieldNames first
      if (nonStaticFieldNames.containsKey(wrapper)) {
        if (qualifiedName.getQualifier.getFullyQualifiedName.compareTo("this") == 0)
          coOccurVars.push("this." + "." + qualifiedName.getName.getIdentifier)
        else
          // it could be object.fieldHere
          coOccurVars.push(qualifiedName.getFullyQualifiedName)
      }else if(staticFieldNames.containsKey(new JDTNameWrapper(qualifiedName.getName))) {
        // the case: A.FIELD_HERE
        if(qualifiedName.getQualifier.resolveTypeBinding().getQualifiedName.compareTo(faultFile) == 0)
          coOccurVars.push(faultFile+"."+qualifiedName.getName.getIdentifier)
        else
          coOccurVars.push(qualifiedName.getFullyQualifiedName)
      }
    }
    false
  }

  import scala.collection.mutable
  // The reason for separation nonstatic vs static fields is because nonstatic field is accessed via
  // this expression, while static field cannot be accessed via this expression. Additionally, non static fields
  // cannot be used in static methods.

  val nonStaticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  val staticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  val localVars = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
  val scope: mutable.Stack[(Int, Int)] = new mutable.Stack()

  //Example:  int zIndex = (getNumObjectiveFunctions() == 1) ? 0 : 1;
  override def visit (node : VariableDeclarationStatement) : Boolean = {
    //println(node)
    true
  }

  // TODO consider constructing graph with this type later
  override def endVisit(node: VariableDeclarationFragment): Unit = {
    constructGraph()
    toProcess = false
  }

  //Example: zIndex = (getNumObjectiveFunctions() == 1) ? 0 : 1
  override def visit(node: VariableDeclarationFragment): Boolean = {
    //println(node)
    scope.top
    toProcess = true
    val binding = node.resolveBinding()
    if (binding.isField) {
      if (!Modifier.isStatic(binding.getModifiers)) {
        addVar(node.getName, nonStaticFieldNames)
      } else {
        addVar(node.getName, staticFieldNames)
      }

    } else {
      // Not field so it is a normal variable, e.g., local variable in a method.
      // It is declared before faulty statement
      addVar(node.getName, localVars)
    }
    true
  }

  //Example: for (int i = 0; i < getNumArtificialVariables(); i++) { ==> int i = 0
  override def visit(node: VariableDeclarationExpression): Boolean = {
    //println(node)
    true
  }

  private def addVar(node: SimpleName, container: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]): Unit = {
    container.get(JDTNameWrapper(node)) match {
      case null => {
        val types = new java.util.ArrayList[ITypeBinding]
        types.add(node.resolveTypeBinding())
        container.put(JDTNameWrapper(node), types)
      }
      case t => t.add(node.resolveTypeBinding())
    }
  }

  private def pushScope(node: ASTNode): (Int, Int) = {
    val scopeBeginLine = ASTUtils.getStatementLineNo(node)
    val scopeEndLine = ASTUtils.getStatementEndLineNo(node)
    scope.push((scopeBeginLine, scopeEndLine))
    (scopeBeginLine, scopeEndLine)
  }

  override def visit(node: ForStatement): Boolean = {
    pushScope(node)
    true
  }

  override def endVisit(node: ForStatement): Unit = {
    scope.pop()
  }

  override def visit(node: MethodDeclaration): Boolean = {
    pushScope(node)

    this.localVars.clear()
    import scala.collection.JavaConversions._
    for (o <- node.parameters) {
      if (o.isInstanceOf[SingleVariableDeclaration]) {
        val v: SingleVariableDeclaration = o.asInstanceOf[SingleVariableDeclaration]
        localVars.get(JDTNameWrapper(v.getName)) match {
          case null =>{
            val types = new java.util.ArrayList[ITypeBinding]
            types.append(v.getName.resolveTypeBinding())
            localVars.put(JDTNameWrapper(v.getName), types)
          }
          case t => t.append(v.getName.resolveTypeBinding())
        }
      }
    }

    true
  }

  override def endVisit(node: MethodDeclaration): Unit = {
    scope.pop()
  }

  override def visit(node: WhileStatement): Boolean = {
    pushScope(node)
    true
  }

  override def endVisit(node: WhileStatement): Unit = {
    scope.pop()
  }

  override def visit(node: AnonymousClassDeclaration): Boolean = {
    pushScope(node)
    true
  }

  override def endVisit(node: AnonymousClassDeclaration): Unit = {
    scope.pop()
  }

  override def visit(node: TypeDeclaration): Boolean = {
    pushScope(node)
    true
  }

  override def endVisit(node: TypeDeclaration): Unit = {
    scope.pop()
  }

  override def visit(node: TryStatement): Boolean ={
    pushScope(node)
    true
  }

  override def endVisit(node: TryStatement): Unit = {
    scope.pop()
  }

  override def visit(node: CatchClause): Boolean ={
    pushScope(node)
    true
  }

  override def endVisit(node: CatchClause): Unit = {
    scope.pop()
  }

  override def visit(node: Block): Boolean ={
    pushScope(node)
    true
  }

  override def endVisit(node: Block): Unit = {
    scope.pop()
  }

  override def visit(node: IfStatement): Boolean ={
    pushScope(node)
    true
  }

  override def endVisit(node: IfStatement): Unit = {
    scope.pop()
  }

  override def visit(node: SwitchCase): Boolean ={
    pushScope(node)
    true
  }

  override def endVisit(node: SwitchCase): Unit = {
    scope.pop()
  }
}
