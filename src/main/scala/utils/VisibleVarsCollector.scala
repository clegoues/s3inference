package utils

import java.util.List

import driver.Options
import frontend.Repairable
import localization.Identifier
import org.eclipse.jdt.core.dom._
import org.eclipse.jdt.core.dom.Modifier

import scala.collection.mutable.ArrayBuffer

/**
 * Created by dxble on 8/4/15.
 */
case class JDTNameWrapper(name: Name, thisExp: Boolean = false) {
    override def equals(obj: Any): Boolean = {
        if(!obj.isInstanceOf[JDTNameWrapper])
            return false
        name.getFullyQualifiedName.compareTo(obj.asInstanceOf[JDTNameWrapper].name.getFullyQualifiedName) == 0
    }

    override def hashCode(): Int = {
        name.getFullyQualifiedName.hashCode
    }

    def getFullyQualifiedName(): String = {
        if(!thisExp)
            name.getFullyQualifiedName
        else "this."+name.getFullyQualifiedName
    }
}
class UsedVariables() extends ASTVisitor{
    val varsUsed = new java.util.HashMap[JDTNameWrapper, ITypeBinding]
    var visitingThisExp = false

    private def toCollectVar(typeBinding: ITypeBinding): Boolean = {
        if(typeBinding == null)
            return false
        if(Options.collectObjectVar)
            if(typeBinding.isClass)
                return true

        return Repairable.canHandleType(typeBinding)
    }

    override def visit(node: ThisExpression): Boolean = {
        visitingThisExp = true
        true
    }

    //override def endVisit(node: ThisExpression) ={
    //    visitingThisExp = false
    //}

    override def visit(node: SimpleName): Boolean = {
        val binding = node.resolveBinding()
        if(binding.isInstanceOf[IVariableBinding]) {
            val typeBinding = node.resolveTypeBinding()
            if(toCollectVar(typeBinding))
                varsUsed.put(JDTNameWrapper(node, visitingThisExp), typeBinding)
        }
        visitingThisExp = false
        false
    }

    override def visit(node: QualifiedName): Boolean = {
        val binding = node.resolveBinding()
        if(binding.isInstanceOf[IVariableBinding]) {
            val typeBinding = node.resolveTypeBinding()
            if(toCollectVar(typeBinding))
                varsUsed.put(JDTNameWrapper(node, visitingThisExp),typeBinding)
        }
        visitingThisExp = false
        false
    }

    def availableVarsAtFault(fault: Identifier[Any]): scala.collection.mutable.Map[JDTNameWrapper, ITypeBinding] = {
        import scala.collection.JavaConversions._
        return varsUsed.filter(v => {
            val modifier = v._1.name.resolveBinding().getModifiers
            if(Modifier.isStatic(modifier)){
                // static fields is available anywhere
                true
            }else {
                // Non static
                var qualifier: Name = null
                var name: Name = null
                if(v._1.name.isQualifiedName) {
                    qualifier = v._1.name.asInstanceOf[QualifiedName].getQualifier
                    name = v._1.name.asInstanceOf[QualifiedName].getName
                }else{
                    // Simple Name
                    name = v._1.name
                }
                // Check non static in current class of fault
                val isNonStaticField1 = fault.getNonStaticFieldVars().exists(p =>
                    p._1.getFullyQualifiedName().compareTo(name.getFullyQualifiedName) == 0 &&
                      (qualifier == null || qualifier.getFullyQualifiedName.compareTo("this") ==0 || fault.isInScope(qualifier)))
                // Check non static field in variable visible at fault
                if(isNonStaticField1)
                    true
                else {
                    // E.g., current Method vars = x of type Example. Then compare with x.
                    // if p._1 is x => ok. p._1 is C.x => not ok
                    val isNonStaticField2 = fault.getLocalVisibleVars().exists(pCurrentMethVar => {
                        // Same name and same type
                        ((qualifier == null && pCurrentMethVar._1.getFullyQualifiedName().compareTo(name.getFullyQualifiedName) == 0) &&
                          pCurrentMethVar._2.exists(ty =>{
                            ty.getQualifiedName.compareTo(v._2.getQualifiedName) == 0
                        })) ||
                           // qualifier same with current method var and Same type
                          ((qualifier != null && pCurrentMethVar._1.getFullyQualifiedName().compareTo(qualifier.getFullyQualifiedName) == 0) &&
                            pCurrentMethVar._2.exists(ty =>{
                                ty.getQualifiedName.compareTo(qualifier.resolveTypeBinding().getQualifiedName) == 0
                            }))
                    })
                    if(isNonStaticField2)
                        true
                    else {
                        false
                    }
                }
            }
        })
    }
}
/*class VisibleVarsCollector(currentNode: Identifier[Any], checkScope: Boolean = true) extends ASTVisitor {
    import org.eclipse.jdt.core.dom.{AbstractTypeDeclaration, AnnotationTypeDeclaration, AnnotationTypeMemberDeclaration, AnonymousClassDeclaration, ArrayAccess, ArrayCreation, ArrayInitializer, ArrayType, AssertStatement, Assignment, ASTNode, Block, BlockComment, BooleanLiteral, BreakStatement, CastExpression, CatchClause, CharacterLiteral, ClassInstanceCreation, CompilationUnit, ConditionalExpression, ConstructorInvocation, ContinueStatement, DoStatement, EmptyStatement, EnhancedForStatement, EnumConstantDeclaration, EnumDeclaration, ExpressionStatement, FieldAccess, FieldDeclaration, ForStatement, IfStatement, ImportDeclaration, InfixExpression, Initializer, InstanceofExpression, Javadoc, LabeledStatement, LineComment, MarkerAnnotation, MemberRef, MemberValuePair, MethodDeclaration, MethodInvocation, MethodRef, MethodRefParameter, Modifier, NormalAnnotation, NullLiteral, NumberLiteral, PackageDeclaration, ParameterizedType, ParenthesizedExpression, PostfixExpression, PrefixExpression, PrimitiveType, QualifiedName, QualifiedType, ReturnStatement, SimpleName, SimpleType, SingleMemberAnnotation, SingleVariableDeclaration, StringLiteral, SuperConstructorInvocation, SuperFieldAccess, SuperMethodInvocation, SwitchCase, SwitchStatement, SynchronizedStatement, TagElement, TextElement, ThisExpression, ThrowStatement, TryStatement, TypeDeclaration, TypeDeclarationStatement, TypeLiteral, TypeParameter, UnionType, VariableDeclarationExpression, VariableDeclarationFragment, VariableDeclarationStatement, WhileStatement, WildcardType}
    val statements = new ArrayBuffer[Identifier[Any]]()
    val nonStaticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    val staticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    val currentMethodVars = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    val currentVarsInForLoop = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    var visitingFaultyMethod = false
    var retrievedVarsInForStmt = false
    var visitedFaultyMethod = false
    var isFaultyMethodStatic = false
    var visitingConfinedScope = false

    private def collectVarsInBoundedForStatement(): Unit ={
        def getBoundedForStmt(): ForStatement = {
            if(currentNode.getJavaNode().isInstanceOf[ForStatement])
                return currentNode.getJavaNode().asInstanceOf[ForStatement]

            var parent = currentNode.getJavaNode().getParent
            while (parent != null && !parent.isInstanceOf[MethodDeclaration] && !parent.isInstanceOf[CompilationUnit]) {
                if (parent.isInstanceOf[ForStatement])
                    return parent.asInstanceOf[ForStatement]
                parent = parent.getParent
            }
            return null
        }
        if(!retrievedVarsInForStmt) {
            retrievedVarsInForStmt = true
            val boundedForStmt = getBoundedForStmt()

            if(boundedForStmt != null) {
                val inits = boundedForStmt.initializers()
                import scala.collection.JavaConversions._
                for(eachInit <- inits){
                    if(eachInit.isInstanceOf[VariableDeclarationExpression]){
                        val varDecFrags = eachInit.asInstanceOf[VariableDeclarationExpression].fragments()
                        import scala.collection.JavaConversions._
                        for (eachFrag <- varDecFrags) {
                            val v = eachFrag.asInstanceOf[VariableDeclarationFragment]
                            currentVarsInForLoop.get(JDTNameWrapper(v.getName)) match {
                                case null => {
                                    val types = new java.util.ArrayList[ITypeBinding]
                                    types.append(v.getName.resolveTypeBinding())
                                    currentVarsInForLoop.put(JDTNameWrapper(v.getName), types)
                                }
                                case t => t.append(v.getName.resolveTypeBinding())
                            }
                        }
                    }
                }
            }
        }
    }


    def collectVisibleVars(node: ASTNode): Boolean ={
        val iden = ASTUtils.getFaultIdentifier(node)
        iden.setJavaNode(node)

        // This is to avoid collecting variables declared after the faulty line
        if(iden.getBeginLine().toInt > currentNode.getBeginLine().toInt)
            return false

        //if(currentNode.getScope().isEmpty)
        //    throw new RuntimeException("Scope for this statement is NULL! "+currentNode)

        //TODO: check scope need to consider field variables (global variables), static field access of a class.
        if(checkScope) {// by default we dont check scope yet
                if (currentNode.getScope() == null) {
                    // to set scope for this curNode
                    currentNode.setScope(ASTUtils.getScope(currentNode.getJavaNode()))
                }
                currentNode.setOtherVisibleVars(currentMethodVars)
                // This is because static method do not allow to access non-static fields
                if(isFaultyMethodStatic)
                    nonStaticFieldNames.clear()

                currentNode.setNonStaticFieldVars(nonStaticFieldNames)
                currentNode.setStaticFieldVars(staticFieldNames)
                if(!retrievedVarsInForStmt && currentNode.getForLoopVars() == null) {
                    collectVarsInBoundedForStatement()
                    currentNode.setforLoopVars(currentVarsInForLoop)
                }
            if (iden.getScope() == null) {
                // to set scope for this curNode
                iden.setScope(ASTUtils.getScope(iden.getJavaNode()))
            }

        }else{
            //statements.append(iden)
        }
        return true
    }
    /*override def visit (node : AnnotationTypeDeclaration) : Boolean = { visitNode(node) }
    override def visit (node : AnnotationTypeMemberDeclaration) : Boolean = { visitNode(node) }
    override def visit (node : AnonymousClassDeclaration) : Boolean = { visitNode(node) }
    override def visit (node : ArrayAccess) : Boolean = { visitNode(node) }
    override def visit (node : ArrayCreation) : Boolean = { visitNode(node) }
    override def visit (node : ArrayInitializer) : Boolean = { visitNode(node) }
    override def visit (node : ArrayType) : Boolean = { visitNode(node) }*/
    override def visit (node : AssertStatement) : Boolean = {
        collectVisibleVars(node); false
    }

    override def visit (node : Assignment) : Boolean = {
        collectVisibleVars(node); false }
    //override def visit (node : Block) : Boolean = { visitNode(node) }
    //override def visit (node : BlockComment) : Boolean = { visitNode(node) }
    //override def visit (node : BooleanLiteral) : Boolean = { visitNode(node) }
    override def visit (node : BreakStatement) : Boolean = {
        collectVisibleVars(node); false}
    //override def visit (node : CastExpression) : Boolean = { visitNode(node) }
    //override def visit (node : CatchClause) : Boolean = { visitNode(node) }
    //override def visit (node : CharacterLiteral) : Boolean = { visitNode(node) }
    //override def visit (node : ClassInstanceCreation) : Boolean = { visitNode(node) }
    //override def visit (node : CompilationUnit) : Boolean = { visitNode(node) }
    //override def visit (node : ConditionalExpression) : Boolean = { visitNode(node) }
    override def visit (node : ConstructorInvocation) : Boolean = {
        collectVisibleVars(node); false
    }
    override def visit (node : ContinueStatement) : Boolean = {
        collectVisibleVars(node); false
    }
    override def visit (node : DoStatement) : Boolean = {
        collectVisibleVars(node); true
    }
    //override def visit (node : EmptyStatement) : Boolean = { visitNode(node) }
    override def visit (node : EnhancedForStatement) : Boolean = {
        collectVisibleVars(node); true
    }
    //override def visit (node : EnumConstantDeclaration) : Boolean = { visitNode(node) }
    //override def visit (node : EnumDeclaration) : Boolean = { visitNode(node) }
    override def visit (node : ExpressionStatement) : Boolean = { collectVisibleVars(node); false }
    //override def visit (node : FieldAccess) : Boolean = { visitNode(node) }
    override def visit (node : FieldDeclaration) : Boolean = {
        val parent=node.getParent
        val parent_start = ASTUtils.getStatementLineNo(parent)
        val parent_end = ASTUtils.getStatementEndLineNo(parent)

        if(currentNode.getLine() >= parent_start && currentNode.getLine() <= parent_end) {
            import scala.collection.JavaConversions._
            for (o <- node.fragments) {
                if (o.isInstanceOf[VariableDeclarationFragment]) {
                    val v: VariableDeclarationFragment = o.asInstanceOf[VariableDeclarationFragment]
                    if(!Modifier.isStatic(node.getModifiers)){
                        nonStaticFieldNames.get(JDTNameWrapper(v.getName)) match {
                            case null => {
                                val types = new java.util.ArrayList[ITypeBinding]
                                types.append(v.getName.resolveTypeBinding())
                                nonStaticFieldNames.put(JDTNameWrapper(v.getName), types)
                            }
                            case t => t.append(v.getName.resolveTypeBinding())
                        }
                    }else{
                        staticFieldNames.get(JDTNameWrapper(v.getName)) match {
                            case null => {
                                val types = new java.util.ArrayList[ITypeBinding]
                                types.append(v.getName.resolveTypeBinding())
                                staticFieldNames.put(JDTNameWrapper(v.getName), types)
                            }
                            case t => t.append(v.getName.resolveTypeBinding())
                        }
                    }
                }
            }
        }
        return super.visit(node)
    }

    override def visit (node : ForStatement) : Boolean = {
        collectVisibleVars(node); true
        /*val iden = ASTUtils.getFaultIdentifier(node)
        iden.setJavaNode(node)
        visitingConfinedScope = true

        // This is to avoid collecting variables declared after the faulty line
        if(iden.getBeginLine().toInt > currentNode.getBeginLine().toInt)
            return false
        val visitor = new VisibleVarsCollector(currentNode, true)
        visitor.visitingFaultyMethod = visitingFaultyMethod
        visitor.visitingConfinedScope = false
        node.accept(visitor)
        import scala.collection.JavaConversions._
        visitor.currentMethodVars.foreach(f => this.currentVarsInForLoop += f)
        false*/
    }

    override def endVisit(node: ForStatement) = {
        visitingConfinedScope = false
    }

    override def visit (node : IfStatement) : Boolean = {
        collectVisibleVars(node); true
    }
    //override def visit (node : ImportDeclaration) : Boolean = { visitNode(node) }
    //override def visit (node : InfixExpression) : Boolean = { visitNode(node) }
    override def visit (node : Initializer) : Boolean = {
        val mods: List[_] = node.modifiers
        import scala.collection.JavaConversions._
        for (o <- mods) {
            if (o.isInstanceOf[Modifier]) {
                if ((o.asInstanceOf[Modifier]).isStatic) {
                    this.currentMethodVars.clear()
                }
            }
        }
        collectVisibleVars(node)
        false
    }
    //override def visit (node : InstanceofExpression) : Boolean = { visitNode(node) }
    //override def visit (node : Javadoc) : Boolean = { visitNode(node) }
    override def visit (node : LabeledStatement) : Boolean = { collectVisibleVars(node); false  }
    //override def visit (node : LineComment) : Boolean = { visitNode(node) }
    //override def visit (node : MarkerAnnotation) : Boolean = { visitNode(node) }
    //override def visit (node : MemberRef) : Boolean = { visitNode(node) }
    //override def visit (node : MemberValuePair) : Boolean = { visitNode(node) }
    override def visit (node : MethodDeclaration) : Boolean = {
        //println(node.getName)
        //println(currentNode.getMethodName())
        val nodeLine=ASTUtils.getStatementLineNo(node)
        val nodeEndline=ASTUtils.getStatementEndLineNo(node)
        /*if(nodeLine >=  298){
            println(node.getName)
            println(currentNode.getMethodName())
        }*/
        if(/*node.getName.equals(currentNode.getMethodName()) &&*/ nodeLine < currentNode.getLine() && nodeEndline >= currentNode.getLine()) {
            if(currentNode.getSurroundingMethod() == null)
                currentNode.setSurroundingMethod(node.resolveBinding())
            this.currentMethodVars.clear()
            import scala.collection.JavaConversions._
            for (o <- node.parameters) {
                if (o.isInstanceOf[SingleVariableDeclaration]) {
                    val v: SingleVariableDeclaration = o.asInstanceOf[SingleVariableDeclaration]
                    currentMethodVars.get(JDTNameWrapper(v.getName)) match {
                        case null =>{
                            val types = new java.util.ArrayList[ITypeBinding]
                            types.append(v.getName.resolveTypeBinding())
                            currentMethodVars.put(JDTNameWrapper(v.getName), types)
                        }
                        case t => t.append(v.getName.resolveTypeBinding())
                    }
                }
            }
            if(Modifier.isStatic(node.getModifiers)){
                isFaultyMethodStatic = true
                currentNode.setMethodIsStatic(true)
            }
            visitingFaultyMethod = true
        }else{
            //TODO: to consier filename later
            visitingFaultyMethod = false
            //currentNodeMethodVector = null
        }

        return true
        //return super.visit(node)
    }

    override def endVisit(node : MethodDeclaration) ={
    }
    //override def visit (node : MethodInvocation) : Boolean = { node.resolveMethodBinding() }
    //override def visit (node : MethodRef) : Boolean = { visitNode(node) }
    //override def visit (node : MethodRefParameter) : Boolean = { visitNode(node) }
    //override def visit (node : Modifier) : Boolean = { visitNode(node) }
    //override def visit (node : NormalAnnotation) : Boolean = { visitNode(node) }
    //override def visit (node : NullLiteral) : Boolean = { visitNode(node) }
    //override def visit (node : NumberLiteral) : Boolean = { visitNode(node) }
    //override def visit (node : PackageDeclaration) : Boolean = { visitNode(node) }
    //override def visit (node : ParameterizedType) : Boolean = { visitNode(node) }
    //override def visit (node : ParenthesizedExpression) : Boolean = { visitNode(node) }
    //override def visit (node : PostfixExpression) : Boolean = { visitNode(node) }
    //override def visit (node : PrefixExpression) : Boolean = { visitNode(node) }
    //override def visit (node : PrimitiveType) : Boolean = { visitNode(node) }
    //override def visit (node : QualifiedName) : Boolean = { visitNode(node) }
    //override def visit (node : QualifiedType) : Boolean = { visitNode(node) }
    override def visit (node : ReturnStatement) : Boolean = {
        collectVisibleVars(node); false
    }
    //override def visit (node : SimpleName) : Boolean = { visitNode(node) }
    //override def visit (node : SimpleType) : Boolean = { visitNode(node) }
    //override def visit (node : SingleMemberAnnotation) : Boolean = { visitNode(node) }
    //override def visit (node : SingleVariableDeclaration) : Boolean = { visitNode(node) }
    //override def visit (node : StringLiteral) : Boolean = { visitNode(node) }
    override def visit (node : SuperConstructorInvocation) : Boolean = {
        collectVisibleVars(node); false
    }
    //override def visit (node : SuperFieldAccess) : Boolean = { visitNode(node) }
    override def visit (node : SuperMethodInvocation) : Boolean = {
        collectVisibleVars(node); false
    }
    //override def visit (node : SwitchCase) : Boolean = { visitNode(node) }
    override def visit (node : SwitchStatement) : Boolean = {
        collectVisibleVars(node); true
    }
    override def visit (node : SynchronizedStatement) : Boolean = {
        collectVisibleVars(node); false
    }
    //override def visit (node : TagElement) : Boolean = { visitNode(node) }
    //override def visit (node : TextElement) : Boolean = { visitNode(node) }
    //override def visit (node : ThisExpression) : Boolean = { visitNode(node) }
    override def visit (node : ThrowStatement) : Boolean = {
        collectVisibleVars(node); false
    }
    override def visit (node : TryStatement) : Boolean = {
        collectVisibleVars(node); true
    }

    //override def visit (node : TypeDeclaration) : Boolean = { visitNode(node) }
    //override def visit (node : TypeDeclarationStatement) : Boolean = { visitNode(node) }
    //override def visit (node : TypeLiteral) : Boolean = { visitNode(node) }
    //override def visit (node : TypeParameter) : Boolean = { visitNode(node) }
    //override def visit (node : UnionType) : Boolean = { visitNode(node) }
    //override def visit (node : VariableDeclarationExpression) : Boolean = { visitNode(node) }
    //override def visit (node : VariableDeclarationFragment) : Boolean = { visitNode(node) }
    override def visit (node : VariableDeclarationStatement) : Boolean = {
        import scala.collection.JavaConversions._
        if(visitingFaultyMethod && !visitingConfinedScope && ASTUtils.getStatementLineNo(node) < currentNode.getBeginLine().toInt){
            for (o <- node.fragments) {
                if (o.isInstanceOf[VariableDeclarationFragment]) {
                    val v: VariableDeclarationFragment = o.asInstanceOf[VariableDeclarationFragment]
                    currentMethodVars.get(JDTNameWrapper(v.getName)) match {
                        case null => {
                            val types = new java.util.ArrayList[ITypeBinding]
                            types.append(v.getName.resolveTypeBinding())
                            currentMethodVars.put(JDTNameWrapper(v.getName), types)
                        }
                        case t => t.append(v.getName.resolveTypeBinding())
                    }
                }
            }
        }else{
        }
        collectVisibleVars(node)
        false
    }
    override def visit (node : WhileStatement) : Boolean = {
        collectVisibleVars(node); true
    }
    //override def visit (node : WildcardType) : Boolean = { visitNode(node) }
}*/

class VisibleVars(currentNode: Identifier[Any]) extends ASTVisitor{
    import scala.collection.mutable
    // The reason for separation nonstatic vs static fields is because nonstatic field is accessed via
    // this expression, while static field cannot be accessed via this expression. Additionally, non static fields
    // cannot be used in static methods.

    val nonStaticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    val staticFieldNames = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    val otherVisibleVars = new java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]
    var visitingFaultyMethod = false
    var retrievedVarsInForStmt = false
    var visitedFaultyMethod = false
    var isFaultyMethodStatic = false
    var visitingConfinedScope = false // e.g., for loop, do while, try catch
    val scope: mutable.Stack[(Int, Int)] = new mutable.Stack()

    //Example:  int zIndex = (getNumObjectiveFunctions() == 1) ? 0 : 1;
    override def visit (node : VariableDeclarationStatement) : Boolean = {
        //println(node)
        true
    }

    //Example: zIndex = (getNumObjectiveFunctions() == 1) ? 0 : 1
    override def visit(node: VariableDeclarationFragment): Boolean = {
        //println(node)
        val (beginScope, endScope) = scope.top

        // Check in scope
        if(beginScope < currentNode.beginLine && endScope >= currentNode.endLine) {
            val binding = node.resolveBinding()
            if (binding.isField) {
                if (!Modifier.isStatic(binding.getModifiers)) {
                    addVisibleVar(node.getName, nonStaticFieldNames)
                } else {
                    addVisibleVar(node.getName, staticFieldNames)
                }

            } else {
                // Not field so it is a normal variable, e.g., local variable in a method.
                // It is declared before faulty statement
                // TODO: for statement has initializers with the same begin line. So we may take initialized variables
                // in forloop for granted. Fix this later.
                if (ASTUtils.getStatementLineNo(node) < currentNode.beginLine && ASTUtils.getStatementEndLineNo(node) <= currentNode.endLine)
                    addVisibleVar(node.getName, otherVisibleVars)
            }
        }
        true
    }

    //Example: for (int i = 0; i < getNumArtificialVariables(); i++) { ==> int i = 0
    override def visit(node: VariableDeclarationExpression): Boolean = {
        //println(node)
        true
    }

    private def addVisibleVar(node: SimpleName, container: java.util.HashMap[JDTNameWrapper, java.util.ArrayList[ITypeBinding]]): Unit = {
        container.get(JDTNameWrapper(node)) match {
            case null => {
                val types = new java.util.ArrayList[ITypeBinding]
                types.add(node.resolveTypeBinding())
                container.put(JDTNameWrapper(node), types)
            }
            case t => t.add(node.resolveTypeBinding())
        }
    }

    private def pushScope(node: ASTNode): (Int, Int) = {
        val scopeBeginLine = ASTUtils.getStatementLineNo(node)
        val scopeEndLine = ASTUtils.getStatementEndLineNo(node)
        scope.push((scopeBeginLine, scopeEndLine))
        (scopeBeginLine, scopeEndLine)
    }

    override def visit(node: ForStatement): Boolean = {
        pushScope(node)
        true
    }

    override def endVisit(node: ForStatement): Unit = {
        scope.pop()
    }

    override def visit(node: MethodDeclaration): Boolean = {
        val (scopeBeginLine, scopeEndLine) = pushScope(node)

        if(scopeBeginLine < currentNode.beginLine && scopeEndLine >= currentNode.endLine){
            if(currentNode.getSurroundingMethod() == null)
                currentNode.setSurroundingMethod(node.resolveBinding())
            this.otherVisibleVars.clear()
            import scala.collection.JavaConversions._
            for (o <- node.parameters) {
                if (o.isInstanceOf[SingleVariableDeclaration]) {
                    val v: SingleVariableDeclaration = o.asInstanceOf[SingleVariableDeclaration]
                    otherVisibleVars.get(JDTNameWrapper(v.getName)) match {
                        case null =>{
                            val types = new java.util.ArrayList[ITypeBinding]
                            types.append(v.getName.resolveTypeBinding())
                            otherVisibleVars.put(JDTNameWrapper(v.getName), types)
                        }
                        case t => t.append(v.getName.resolveTypeBinding())
                    }
                }
            }
            if(Modifier.isStatic(node.getModifiers)){
                isFaultyMethodStatic = true
                currentNode.setMethodIsStatic(true)
            }
            visitingFaultyMethod = true
        }else{
            visitingFaultyMethod = false
        }
        true
    }

    override def endVisit(node: MethodDeclaration): Unit = {
        scope.pop()
    }

    override def visit(node: WhileStatement): Boolean = {
        pushScope(node)
        true
    }

    override def endVisit(node: WhileStatement): Unit = {
        scope.pop()
    }

    override def visit(node: AnonymousClassDeclaration): Boolean = {
        pushScope(node)
        true
    }

    override def endVisit(node: AnonymousClassDeclaration): Unit = {
        scope.pop()
    }

    override def visit(node: TypeDeclaration): Boolean = {
        pushScope(node)
        true
    }

    override def endVisit(node: TypeDeclaration): Unit = {
        scope.pop()
    }

    override def visit(node: TryStatement): Boolean ={
        pushScope(node)
        true
    }

    override def endVisit(node: TryStatement): Unit = {
        scope.pop()
    }

    override def visit(node: CatchClause): Boolean ={
        pushScope(node)
        true
    }

    override def endVisit(node: CatchClause): Unit = {
        scope.pop()
    }

    override def visit(node: Block): Boolean ={
        pushScope(node)
        true
    }

    override def endVisit(node: Block): Unit = {
        scope.pop()
    }

    override def visit(node: IfStatement): Boolean ={
        pushScope(node)
        true
    }

    override def endVisit(node: IfStatement): Unit = {
        scope.pop()
    }

    override def visit(node: SwitchCase): Boolean ={
        pushScope(node)
        true
    }

    override def endVisit(node: SwitchCase): Unit = {
        scope.pop()
    }
}