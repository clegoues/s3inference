package utils.analysis

import localization.FaultIdentifier
import org.eclipse.jdt.core.dom._
import driver.S3Properties
import frontend.Repairable
import utils.ASTUtils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by dxble on 2/26/17.
  */
class PrioritizeSearchSpace {
  class VariableNamesCollector extends ASTVisitor{
    val names = new scala.collection.mutable.HashMap[String, ITypeBinding]()
    override def visit(node: SimpleName): Boolean ={
      names += node.getIdentifier -> node.resolveTypeBinding()
      false
    }

    override def visit(node: QualifiedName): Boolean ={
      names += node.getName.getIdentifier -> node.resolveTypeBinding()
      false
    }
  }

  class ContainsVar(names: scala.collection.mutable.HashMap[String, ITypeBinding]) extends ASTVisitor{
    var contain = false
    override def visit(node: SimpleName): Boolean ={
      names.get(node.getIdentifier) match {
        case Some(e) => {
          if(e.toString.compareTo(node.resolveTypeBinding().toString) == 0)
            contain = true
        }
        case _ => {}
      }
      false
    }

    override def visit(node: QualifiedName): Boolean ={
      names.get(node.getName.getIdentifier) match {
        case Some(e) => {
          if(e.toString.compareTo(node.resolveTypeBinding().toString) == 0)
            contain = true
        }
        case _ => {}
      }
      false
    }
  }

  class AnalyzeContext(names: scala.collection.mutable.HashMap[String, ITypeBinding], fault: FaultIdentifier, faultTypeBind: ITypeBinding) extends ASTVisitor{
    val operators = new mutable.Stack[InfixExpression.Operator]()

    def sameFaultNode(node: ASTNode): Boolean = {
      if(fault.sameLocation(node))
        true
      else false
    }

    override def visit(node: InfixExpression): Boolean ={
      if(sameFaultNode(node))
        return false
      else {
        var rootInfix: ASTNode = node
        while(rootInfix.getParent().isInstanceOf[InfixExpression]){
          rootInfix = rootInfix.getParent
        }
        val rootInfixType = rootInfix.asInstanceOf[Expression].resolveTypeBinding()
        if (Repairable.canHandleType(node.resolveTypeBinding()) && rootInfixType.isSubTypeCompatible(faultTypeBind)){
          val containsVarLeft = new ContainsVar(names)
          node.getLeftOperand.accept(containsVarLeft)
          val containsVarRight = new ContainsVar(names)
          node.getRightOperand.accept(containsVarRight)
          if (containsVarLeft.contain || containsVarRight.contain) {
            operators.push(node.getOperator)
          }
        }
        return true
      }
    }
  }

  def getSurroundingMethod(node: ASTNode) = {
    var parent : ASTNode = node.getParent
    while(!parent.isInstanceOf[MethodDeclaration]){
      parent = parent.getParent
    }
    parent
  }

  val prioritizedSpace = mutable.HashMap[FaultIdentifier, mutable.Queue[S3Properties.SynthesisLevel.SynthLevel]]()

  def operatorMapper(op: InfixExpression.Operator): S3Properties.SynthesisLevel.SynthLevel = {
    if(op == InfixExpression.Operator.CONDITIONAL_OR || op == InfixExpression.Operator.CONDITIONAL_AND){
      S3Properties.SynthesisLevel.BASIC_LOGIC
    }else if(op == InfixExpression.Operator.GREATER || op == InfixExpression.Operator.GREATER_EQUALS ||
      op == InfixExpression.Operator.LESS || op == InfixExpression.Operator.LESS_EQUALS){
      S3Properties.SynthesisLevel.BASIC_INEQUALITIES
    }else if(op == InfixExpression.Operator.EQUALS || op == InfixExpression.Operator.NOT_EQUALS){
      S3Properties.SynthesisLevel.BASIC_EQUALITIES
    }else if(op == InfixExpression.Operator.PLUS || op == InfixExpression.Operator.MINUS){
      S3Properties.SynthesisLevel.BASIC_ARITHMETIC
    }else null
  }

  def analyzeSurroundingContext(faultGroup: ArrayBuffer[FaultIdentifier]): Unit ={
    if(!driver.Options.repairNullness) {
      faultGroup.foreach(f => {
        val method = getSurroundingMethod(f.getJavaNode())
        val nameCollector = new VariableNamesCollector()
        f.getJavaNode().accept(nameCollector)
        val typeBindingFault = if (f.getJavaNode().isInstanceOf[ExpressionStatement])
          f.getJavaNode().asInstanceOf[ExpressionStatement].getExpression.resolveTypeBinding()
        else f.getJavaNode().asInstanceOf[Expression].resolveTypeBinding()

        val contextAnalyzer = new AnalyzeContext(nameCollector.names, f, typeBindingFault)
        method.accept(contextAnalyzer)
        val spaces = new mutable.Queue[S3Properties.SynthesisLevel.SynthLevel]()
        while (!contextAnalyzer.operators.isEmpty) {
          val op = contextAnalyzer.operators.pop()
          val synthLevel = operatorMapper(op)
          if (op != null)
            spaces.enqueue(synthLevel)
        }
        prioritizedSpace += f -> spaces
      })
    }
  }
}
