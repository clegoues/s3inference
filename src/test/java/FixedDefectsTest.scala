import java.io.{File, FileWriter}

import driver.ConfigurationProperties

/**
  * Created by dxble on 2/9/17.
  */

object FixedDefectsTest{
  private var solver: String =  "CVC4" // CVC4, Enum, Meta, Angelix
  private var file: String = null
  // t5 Clo 62
  private val benchmark: Array[String] = Array[String](
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-lang_30/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_4/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/rtree_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/jflex_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/Closure_62/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/fyodor_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/SimpleFlatMapper_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/graphhopper_4/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/natty_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_20/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/heritrix3_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/engine.io-client.java_2/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/ews-java-api_4/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/orientdb_8/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/qabel-core_4/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_14/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/kraken_4/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_33d4j/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_82d4j/config.prop",
    "/home/dxble/workspace/defectmybugs/clones/chosen/extract/exp4j_4/config.prop"
  )
  def main(args: Array[String]): Unit = {
    //test1() // Enum[no] CVC4[no] Angelix[no] Meta[yes, semantically equiv]
    //test2() // Enum[yes, same] CVC4[yes, same] Angelix[yes, same] Meta[yes, same]
    //test3() // Enum[overfit] CVC4[overfit] Angelix[overfit] Meta[yes, same]
    //test4() // Enum[yes, same] CVC4[yes, same] Angelix[no, timedout, 7 tests, long spec] Meta[yes, same]
    //test5() // Enum[overfit] CVC4[overfit] Angelix[overfit] Meta[yes, same]
    //test6() // Enum[yes, same] CVC4[yes, same] Angelix[yes, same] Meta[yes, same]
    //test7() // Enum[yes, semantically same] CVC4[yes] Angelix[overfit] Meta[yes, same]
    //test8() // Enum[overfit] CVC4[overfit] Angelix[overfit] Meta[yes, same]
    //test9() // Enum[overfit] CVC4[overfit] Angelix[overfit] Meta[yes, same]
    //test10() // Enum[overfit] CVC4[overfit] Angelix[overfit] Meta[yes, same]
    //test11() // heritrix3 // Enum[error, likely beautifier] CVC4[error] Angelix[no, delete branch] Meta[yes, same]
    //test12() // Enum[no] CVC4[no] Angelix[no] Meta[yes, same]
    //test13() // Enum[yes, same] CVC4[yes, same] Angelix[overfit] Meta[yes, same]
    //test14() // Enum[yes] CVC4[no] Angelix[yes, same] Meta[yes, same]
    //test15() // Enum[overfit] CVC4[overfit] Angelix[yes, same] Meta[yes, same]
    //test16() // Enum[no] CVC4[no] Angelix[no] Meta[yes, same]
    //test17() // Enum[overfit] CVC4[overfit] Angelix[yes, same] Meta[yes, same]
    //test18() // Enum[no] CVC4[no] Angelix[yes, same] Meta[yes, same]
    //test19() // Enum[no, fail] CVC4[no] Angelix[no, give same buggy exp] Meta[yes, same] //Note when collecting runtime, Integer = null may cause ?UNKNOWN?intValue()
    //test20() // Enum[no] CVC4[no] Angelix[no] Meta[yes, same]

    //runSyntaxOnly(1)// Syntax[no]
    //runSyntaxOnly(2)// Syntax[yes, same]
    //runSyntaxOnly(3)// Syntax[overfit]
    //runSyntaxOnly(4)// Syntax[yes]
    //runSyntaxOnly(5)// Syntax[yes]
    //runSyntaxOnly(6)// Syntax[yes]                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    //runSyntaxOnly(7)// Syntax[yes]
    //runSyntaxOnly(8)// Syntax[no, timeout] [yes previously]
    //runSyntaxOnly(9)// Syntax[yes]
    //runSyntaxOnly(10)// Syntax[yes]
    //runSyntaxOnly(11)// Syntax[overfit]
    //runSyntaxOnly(12)// Syntax[no]
    //runSyntaxOnly(13)// Syntax[yes]
    //runSyntaxOnly(14)// Syntax[yes]
    //runSyntaxOnly(15)// Syntax[yes]
    //runSyntaxOnly(16)// Syntax[yes]
    //runSyntaxOnly(17)// Syntax[overfit]
    //runSyntaxOnly(18)// Syntax[no]
    //runSyntaxOnly(19)// Syntax[yes]
    //runSyntaxOnly(20)// Syntax[no]

    //runSemanticOnly(1)// Sem[yes]
    //runSemanticOnly(2)// Sem[yes]
    //runSemanticOnly(3)// Sem[overfit]
    //runSemanticOnly(4)// Sem[yes]
    //runSemanticOnly(5)// Sem[no, time]
    //runSemanticOnly(6)// Sem[yes]
    //runSemanticOnly(7)// Sem[yes, interesting, syntactically different]
    //runSemanticOnly(8)// Sem[no, time]
    //runSemanticOnly(9)// Sem[no, exception, time]
    //runSemanticOnly(10)// Sem[no, time <= yes, interesting]
    //runSemanticOnly(11)// Sem[yes, interesting, semantically correct, even better than syntax+sem]
    //runSemanticOnly(12)// Sem[yes]
    //runSemanticOnly(13)// Sem[yes]
    //runSemanticOnly(14)// Sem[yes]
    //runSemanticOnly(15)// Sem[no, not pass tests]
    //runSemanticOnly(16)// Sem[no <= yes]
    //runSemanticOnly(17)// Sem[yes, interesting, syntactically different]
    //runSemanticOnly(18)// Sem[no, not pass tests]
    //runSemanticOnly(19)// Sem[yes]
    //runSemanticOnly(20)// Sem[no, exception]

    //runNoFeature(1)// no
    //runNoFeature(2)// yes
    //runNoFeature(3)// overfit
    //runNoFeature(4)// yes
    //runNoFeature(5)// no time
    //runNoFeature(6)// yes
    //runNoFeature(7)// overfit
    //runNoFeature(8)// no time
    //runNoFeature(9)// no time
    //runNoFeature(10)// no time
    //runNoFeature(11)// yes sem equiv
    //runNoFeature(12)// no
    //runNoFeature(13)// yes
    //runNoFeature(14)// overfit
    //runNoFeature(15)// no, exception, look overfit
    //runNoFeature(16)// no
    //runNoFeature(17)// overfit
    //runNoFeature(18)// yes, sem equiv
    //runNoFeature(19)// overfit
    //runNoFeature(20)// no not pass test
  }

  def runNoFeature(testIndex: Int): Unit = {
    solver = "Meta"
    file=benchmark(testIndex-1)
    if((new File(file+("_nofeature")).exists())) {
      run(file+("_nofeature"))
      return
    }

    ConfigurationProperties.load(file)
    ConfigurationProperties.properties.put("feature.semantic", "false")
    ConfigurationProperties.properties.put("feature.syntax", "false")
    ConfigurationProperties.properties.store(new FileWriter(file+("_nofeature")), "")
    ConfigurationProperties.clearConfig()
    run(file+("_nofeature"))
  }

  def runSyntaxOnly(testIndex: Int): Unit = {
    solver = "Meta"
    file=benchmark(testIndex-1)
    if((new File(file+("_syntax")).exists())) {
      run(file+("_syntax"))
      return
    }

    ConfigurationProperties.load(file)
    ConfigurationProperties.properties.put("feature.semantic", "false")
    ConfigurationProperties.properties.store(new FileWriter(file+("_syntax")), "")
    ConfigurationProperties.clearConfig()
    run(file+("_syntax"))
  }

  def runSemanticOnly(testIndex: Int): Unit = {
    solver = "Meta"
    file=benchmark(testIndex-1)
    if((new File(file+("_semantic")).exists())) {
      run(file+("_semantic"))
      return
    }

    ConfigurationProperties.load(file)
    ConfigurationProperties.properties.put("feature.syntax", "false")
    ConfigurationProperties.properties.store(new FileWriter(file+("_semantic")), "")
    ConfigurationProperties.clearConfig()
    run(file+("_semantic"))
  }

  def test1(): Unit ={
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-lang_30/config.prop"
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-lang_30/config.prop"))
    run()
  }

  def test2(): Unit ={
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_4/config.prop"
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_4/config.prop"))
    FixedDefectsTest.run()
  }

  def test3(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/rtree_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/rtree_2/config.prop"))
  }

  def test4(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/jflex_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/jflex_2/config.prop"))
  }

  def test5(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/Closure_62/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/Closure_62/config.prop"))
  }

  def test6(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/fyodor_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/fyodor_2/config.prop"))
  }

  def test7(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/SimpleFlatMapper_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/SimpleFlatMapper_2/config.prop"))
  }

  def test8(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/graphhopper_4/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/graphhopper_4/config.prop"))
  }

  def test9(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/natty_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/natty_2/config.prop"))
  }

  def test10(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_20/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_20/config.prop"))
  }

  def test11(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/heritrix3_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/heritrix3_2/config.prop"))
  }

  def test12(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/engine.io-client.java_2/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/engine.io-client.java_2/config.prop"))
  }

  def test13(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/ews-java-api_4/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/ews-java-api_4/config.prop"))
  }

  def test14(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/orientdb_8/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/orientdb_8/config.prop"))
  }

  def test15(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/qabel-core_4/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/qabel-core_4/config.prop"))
  }

  def test16(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_14/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/molgenis_14/config.prop"))
  }

  def test17(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/kraken_4/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/kraken_4/config.prop"))
  }

  def test18(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_33d4j/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/kraken_4/config.prop"))
  }

  def test19(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/commons-math_82d4j/config.prop"
    FixedDefectsTest.run()
    //driver.Main.main(Array("/home/dxble/workspace/defectmybugs/clones/chosen/extract/kraken_4/config.prop"))
  }

  def test20(): Unit = {
    file="/home/dxble/workspace/defectmybugs/clones/chosen/extract/exp4j_4/config.prop"
    FixedDefectsTest.run()
  }

  private def run(): Unit = {
    if(solver.compareTo("Meta") == 0)
      run(file)
    else if(solver.compareTo("CVC4") == 0)
      runCVC4(file)
    else if(solver.compareTo("Enum") == 0)
      runEnum(file)
    else if(solver.compareTo("Angelix") == 0)
      runAngelix(file)
  }

  private def run(file: String): Unit ={
    driver.Main.main(Array[String](file))
  }

  private def runAngelix(fileName: String): Unit = {
    if((new File(fileName.+("_angelix")).exists())) {
      FixedDefectsTest.run(fileName.+("_angelix"))
      return
    }

    ConfigurationProperties.load(fileName)
    ConfigurationProperties.properties.put("synthesisEngine", "angelixMaxSat")
    ConfigurationProperties.properties.put("synthesisJar", "/home/dxble/workspace/repairtools/angelix/src/synthesis/target/scala-2.10/repair-maxsat-assembly-1.0.jar")
    ConfigurationProperties.properties.store(new FileWriter(fileName.+("_angelix")), "")
    ConfigurationProperties.clearConfig()
    FixedDefectsTest.run(fileName.+("_angelix"))
  }

  private def runCVC4(fileName: String): Unit = {
    if((new File(fileName.+("_cvc4")).exists())) {
      FixedDefectsTest.run(fileName.+("_cvc4"))
      return
    }

    ConfigurationProperties.load(fileName)
    ConfigurationProperties.properties.put("synthesisEngine", "cvc4")
    ConfigurationProperties.properties.put("solverPath", "/home/dxble/workspace/synthesis/sygus-comp14/solvers/CVC4-0205-4/cvc4")
    ConfigurationProperties.properties.put("beautifierPath", "/home/dxble/workspace/repairtools/angelix/src/af2sygus/sygusResult/parseResult.py")
    ConfigurationProperties.properties.store(new FileWriter(fileName.+("_cvc4")), "")
    ConfigurationProperties.clearConfig()
    FixedDefectsTest.run(fileName.+("_cvc4"))
  }

  private def runEnum(fileName: String): Unit = {
    if((new File(fileName.+("_enum")).exists())) {
      FixedDefectsTest.run(fileName.+("_enum"))
      return
    }

    ConfigurationProperties.load(fileName)
    ConfigurationProperties.properties.put("synthesisEngine", "enum")
    ConfigurationProperties.properties.put("solverPath", "/home/dxble/workspace/synthesis/sygus-comp14/solvers/enumerative/esolver-synth-lib/bin/debug/esolver-synthlib")
    ConfigurationProperties.properties.put("beautifierPath", "/home/dxble/workspace/repairtools/angelix/src/af2sygus/sygusResult/parseResult.py")
    ConfigurationProperties.properties.store(new FileWriter(fileName.+("_enum")), "")
    ConfigurationProperties.clearConfig()
    FixedDefectsTest.run(fileName.+("_enum"))
  }
}
